import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import Common from "./Common";
import Comment from "./Comment";
class Contact extends Component{
  
  state ={
    address:[],
  }
async componentDidMount() {
   const url = "http://digitalsupporter.in/reactjs/allApi/fatchdada.php";
   const response = await fetch(url);
   const data = await response.json();
   console.log(data[0].phone);
    // this.setState({address:data[0].address,phone:data[0].phone})
    this.setState({address:data})
  //  console.log(address);
  // data.forEach(element => {
    
  // });
}
 
    render(){
      const address=this.state.address
      console.log(55);
      console.log(address);
        return(
			<>
            <Common
    title="Nous contacter"
  />
  <section id="main-container" className="main-container">
    <div className="container">
      <div className="row">
        <div className="col text-center">
          <div className="section-heading">
            <h2>
              <small>Nous contacter</small>
              Nos<span> coordonnées</span>
            </h2>
            <span className="animate-border tw-mt-20 tw-mb-40 ml-auto mr-auto" />
          </div>
          {/* End Heading */}
        </div>
        {/* End Col */}
      </div>
      {/* End Title Row */}
      <div className="row">
      {
                address.map((address , index)=><>
                <div className="col-md-4" key={address.id}>
          <div className="tw-contact-box">
            <div className="contact-heading">
              <img src="assets/images/icon/place1.png" alt className="img-fluid" />
              <h3>New York</h3>
            </div>
            {/* End Content Heading */}
            <div className="contact-info-box-content">
              <i className="fa fa-map-marker" />
              <p>
               {address.address}
              </p>
               {/* <p>{this.state.address}</p>  */}
              <i className="fa fa-phone" />
              <p>{address.phone}</p>
              <i className="fa fa-envelope" />
              <p>{address.mail}</p>
            </div>
            {/* End content info box */}
          </div>
          {/* End Contact Box */}
        </div>
        {/* End Col */}</>)
              } 
        
        <div className="col-md-4">
          <div className="tw-contact-box">
            <div className="contact-heading">
              <img src="assets/images/icon/place3.png" alt className="img-fluid" />
              <h3>China</h3>
            </div>
            {/* End Content Heading */}
            <div className="contact-info-box-content">
              <i className="fa fa-map-marker" />
              <p>Level 14, 388 George Street New York 200</p>
              <i className="fa fa-phone" />
              <p>009-215-5596</p>
              <i className="fa fa-envelope" />
              <p>mail@example.com</p>
            </div>
            {/* End content info box */}
          </div>
          {/* End Contact Box */}
        </div>
        {/* End Col */}
      </div>
      {/* End Row */}
      <div className="row">
        <div className="col">
          <div id="map" className="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7775.433880827984!2d77.65610293488773!3d12.989947700000009!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1149743478a9%3A0x76f5a810c3ad055b!2strinity%20garden%20apartment!5e0!3m2!1sen!2sin!4v1612591351657!5m2!1sen!2sin" width="100%" height={450} frameBorder={0} style={{border: 0}} allowFullScreen aria-hidden="false" tabIndex={0} />
          </div>
        </div>
      </div>
      {/* End Row */}
    </div>
    {/* Container End */}
  </section>
  {/* Contact End */}
  <section id="tw-contact-us" className="tw-contact-us bg-offwhite">
    <div className="container">
      <div className="row">
        <div className="col">
          <div className="section-heading text-center">
            <h2>
              <small>laisser un message</small>
              Donner un <span>Message</span>
            </h2>
            <span className="animate-border border-ash ml-auto mr-auto tw-mt-20 tw-mb-40" />
          </div>
        </div>
        {/* Col End */}
      </div>
      {/* Row End */}
      <div className="contact-us-form">
        <form id="contact-form" className="contact-form" action="#" method="POST">
          <div className="error-container" />
          <div className="row">
            <div className="col-lg-6">
              <div className="form-group">
                <input className="form-control form-name" name="name" id="name" placeholder="Nom" type="text" required />
              </div>
            </div>
            {/* Col end */}
            <div className="col-lg-6">
              <div className="form-group">
                <input className="form-control form-phone" name="phone" id="phone" placeholder="Téléphoner" type="phone" />
              </div>
            </div>
            <div className="col-lg-6">
              <div className="form-group">
                <input className="form-control form-email" name="email" id="email" placeholder="E-mail" type="email" required />
              </div>
            </div>
            <div className="col-lg-6">
              <div className="form-group">
                <input className="form-control form-subject" placeholder="Matière" name="subject" id="subject" type="text" />
              </div>
            </div>
            <div className="col-lg-12">
              <div className="form-group">
                <textarea className="form-control form-message required-field" id="message" placeholder="commentaires" rows={5} defaultValue={""} />
              </div>
            </div>
            {/* Col 12 end */}
          </div>
          {/* Form row end */}
          <div className="text-center">
            <button className="btn btn-primary tw-mt-30" type="submit">Nous contacter</button>
          </div>
        </form>
        {/* Form end */}
      </div>
      {/* Contact us form end */}
    </div>
    {/* Container End */}
  </section>
  {/* Contact End */}
  <Comment/>
  {/* TEstimonial end */}
</>

        )
    }
}
export default Contact