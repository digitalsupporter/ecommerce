import React,{Component} from 'react';
import {Redirect, Link} from 'react-router-dom';
import { withCookies } from "react-cookie";
class Auth extends  Component{
    state = {
      all:[],
      redirect: false,
      authError: false,
      isLoading: false,
    };
    renderRedirect = () => {
      if (this.state.redirect) {
          return <Redirect to='/login'/>
      }
    };
    
    render(){
        let user = this.props.cookies.get("user");
      
        if(user){
        }else{
          this.setState({redirect: true, isLoading: false});
        }
          return(
           <div>
              <>
      
      {this.renderRedirect()}
            </>

         </div>
          )
    }
}
export default withCookies(Auth)