import React, {Component, useEffect} from 'react';
import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
const Featured = (props) => {
    return(
            <>
<div className="featured-products-wrapper py-3">
      <div className="container">
        <div className="section-heading d-flex align-items-center justify-content-between">
          <h6>Featured Products</h6>
          <Link className="btn btn-warning btn-sm" to="featured-products.html">
            View All
          </Link>
        </div>
        <div className="row g-3">
          {/* Featured Product Card*/}
          <div className="col-6 col-md-4 col-lg-3">
            <div className="card featured-product-card">
              <div className="card-body">
                <span className="badge badge-warning custom-badge">
                  <i className="lni lni-star" />
                </span>
                <div className="product-thumbnail-side">
                  <Link className="wishlist-btn" to="#">
                    <i className="lni lni-heart" />
                  </Link>
                  <Link
                    className="product-thumbnail d-block"
                    to="/single"
                  >
                    <img src="assets/img/product/14.png" alt />
                  </Link>
                </div>
                <div className="product-description">
                  <Link
                    className="product-title d-block"
                    to="/single"
                  >
                    Blue Skateboard
                  </Link>
                  <p className="sale-price">
                    $64<span>$89</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* Featured Product Card*/}
          <div className="col-6 col-md-4 col-lg-3">
            <div className="card featured-product-card">
              <div className="card-body">
                <span className="badge badge-warning custom-badge">
                  <i className="lni lni-star" />
                </span>
                <div className="product-thumbnail-side">
                  <Link className="wishlist-btn" to="#">
                    <i className="lni lni-heart" />
                  </Link>
                  <Link
                    className="product-thumbnail d-block"
                    to="/single"
                  >
                    <img src="assets/img/product/15.png" alt />
                  </Link>
                </div>
                <div className="product-description">
                  <Link
                    className="product-title d-block"
                    to="/single"
                  >
                    Travel Bag
                  </Link>
                  <p className="sale-price">
                    $64<span>$89</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* Featured Product Card*/}
          <div className="col-6 col-md-4 col-lg-3">
            <div className="card featured-product-card">
              <div className="card-body">
                <span className="badge badge-warning custom-badge">
                  <i className="lni lni-star" />
                </span>
                <div className="product-thumbnail-side">
                  <Link className="wishlist-btn" to="#">
                    <i className="lni lni-heart" />
                  </Link>
                  <Link
                    className="product-thumbnail d-block"
                    to="/single"
                  >
                    <img src="assets/img/product/16.png" alt />
                  </Link>
                </div>
                <div className="product-description">
                  <Link
                    className="product-title d-block"
                    to="/single"
                  >
                    Cotton T-shirts
                  </Link>
                  <p className="sale-price">
                    $64<span>$89</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* Featured Product Card*/}
          <div className="col-6 col-md-4 col-lg-3">
            <div className="card featured-product-card">
              <div className="card-body">
                <span className="badge badge-warning custom-badge">
                  <i className="lni lni-star" />
                </span>
                <div className="product-thumbnail-side">
                  <Link className="wishlist-btn" to="#">
                    <i className="lni lni-heart" />
                  </Link>
                  <Link
                    className="product-thumbnail d-block"
                    to="/single"
                  >
                    <img src="assets/img/product/6.png" alt />
                  </Link>
                </div>
                <div className="product-description">
                  <Link
                    className="product-title d-block"
                    to="/single"
                  >
                    Roof Lamp{" "}
                  </Link>
                  <p className="sale-price">
                    $64<span>$89</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  
  
  </>

	
  )
}

export default Featured