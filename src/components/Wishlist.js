import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Common from "./Common";
import Back from "./Back";
import Preloader from "./Preloader";
import Sidenav from "./Sidenav";
class Wishlist extends Component{
    render(){
        return(
            <>
  {/* Preloader*/}
            <Preloader/>
  {/* Internet Connection Status*/}
  <div className="internet-connection-status" id="internetStatus" />
  {/* Header Area*/}
 
  <Back
  title="Wishlist"
  />
  
        {/* Sidenav Black Overlay*/}
        <Sidenav/>
        
  <div className="page-content-wrapper">
    {/* Top Products*/}
    <div className="top-products-area py-3">
      <div className="container">
        <div className="section-heading d-flex align-items-center justify-content-between">
          <h6>Your Wishlist (4)</h6>
          {/* Layout Options*/}
          <div className="layout-options">
            <a href="wishlist-grid.html">
              <i className="lni lni-grid-alt" />
            </a>
            <a className="active" href="wishlist-list.html">
              <i className="lni lni-radio-button" />
            </a>
          </div>
        </div>
        <div className="row g-3">
          {/* Single Weekly Product Card*/}
          <div className="col-12 col-md-6">
            <div className="card weekly-product-card">
              <div className="card-body d-flex align-items-center">
                <div className="product-thumbnail-side">
                  <span className="badge badge-success">Sale</span>
                  <a className="wishlist-btn" href="#">
                    <i className="lni lni-heart" />
                  </a>
                  <a
                    className="product-thumbnail d-block"
                    href="/single"
                  >
                    <img src="assets/img/product/10.png" alt />
                  </a>
                </div>
                <div className="product-description">
                  <a
                    className="product-title d-block"
                    href="/single"
                  >
                    Modern Red Sofa
                  </a>
                  <p className="sale-price">
                    <i className="lni lni-dollar" />
                    $64<span>$89</span>
                  </p>
                  <div className="product-rating">
                    <i className="lni lni-star-filled" />
                    4.88 (39)
                  </div>
                  <a className="btn btn-success btn-sm" href="#">
                    <i className="me-1 lni lni-cart" />
                    Buy Now
                  </a>
                </div>
              </div>
            </div>
          </div>
          {/* Single Weekly Product Card*/}
          <div className="col-12 col-md-6">
            <div className="card weekly-product-card">
              <div className="card-body d-flex align-items-center">
                <div className="product-thumbnail-side">
                  <span className="badge badge-primary">Sale</span>
                  <a className="wishlist-btn" href="#">
                    <i className="lni lni-heart" />
                  </a>
                  <a
                    className="product-thumbnail d-block"
                    href="/single"
                  >
                    <img src="assets/img/product/7.png" alt />
                  </a>
                </div>
                <div className="product-description">
                  <a
                    className="product-title d-block"
                    href="/single"
                  >
                    Office Chair
                  </a>
                  <p className="sale-price">
                    <i className="lni lni-dollar" />
                    $100<span>$160</span>
                  </p>
                  <div className="product-rating">
                    <i className="lni lni-star-filled" />
                    4.82 (125)
                  </div>
                  <a className="btn btn-success btn-sm" href="#">
                    <i className="me-1 lni lni-cart" />
                    Buy Now
                  </a>
                </div>
              </div>
            </div>
          </div>
          {/* Single Weekly Product Card*/}
          <div className="col-12 col-md-6">
            <div className="card weekly-product-card">
              <div className="card-body d-flex align-items-center">
                <div className="product-thumbnail-side">
                  <span className="badge badge-danger">-10%</span>
                  <a className="wishlist-btn" href="#">
                    <i className="lni lni-heart" />
                  </a>
                  <a
                    className="product-thumbnail d-block"
                    href="/single"
                  >
                    <img src="assets/img/product/12.png" alt />
                  </a>
                </div>
                <div className="product-description">
                  <a
                    className="product-title d-block"
                    href="/single"
                  >
                    Sun Glasses
                  </a>
                  <p className="sale-price">
                    <i className="lni lni-dollar" />
                    $24<span>$32</span>
                  </p>
                  <div className="product-rating">
                    <i className="lni lni-star-filled" />
                    4.79 (63)
                  </div>
                  <a className="btn btn-success btn-sm" href="#">
                    <i className="me-1 lni lni-cart" />
                    Buy Now
                  </a>
                </div>
              </div>
            </div>
          </div>
          {/* Single Weekly Product Card*/}
          <div className="col-12 col-md-6">
            <div className="card weekly-product-card">
              <div className="card-body d-flex align-items-center">
                <div className="product-thumbnail-side">
                  <span className="badge badge-warning">New</span>
                  <a className="wishlist-btn" href="#">
                    <i className="lni lni-heart" />
                  </a>
                  <a
                    className="product-thumbnail d-block"
                    href="/single"
                  >
                    <img src="assets/img/product/13.png" alt />
                  </a>
                </div>
                <div className="product-description">
                  <a
                    className="product-title d-block"
                    href="/single"
                  >
                    Wall Clock
                  </a>
                  <p className="sale-price">
                    <i className="lni lni-dollar" />
                    $31<span>$47</span>
                  </p>
                  <div className="product-rating">
                    <i className="lni lni-star-filled" />
                    4.99 (7)
                  </div>
                  <a className="btn btn-success btn-sm" href="#">
                    <i className="me-1 lni lni-cart" />
                    Buy Now
                  </a>
                </div>
              </div>
            </div>
          </div>
          {/* Select All Products*/}
          <div className="col-12">
            <div className="select-all-products-btn">
              <a className="btn btn-danger w-100" href="#">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={14}
                  height={14}
                  fill="currentColor"
                  className="bi bi-cart-check me-1"
                  viewBox="0 0 16 16"
                >
                  <path d="M11.354 6.354a.5.5 0 0 0-.708-.708L8 8.293 6.854 7.146a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z" />
                  <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                </svg>
                Add All To Cart
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {/* Internet Connection Status*/}
  <div className="internet-connection-status" id="internetStatus" />
  <Common/>
</>


            )
    }
}
export default Wishlist