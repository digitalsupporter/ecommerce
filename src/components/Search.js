import React, {Component} from 'react';

import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

const Search = (props) => {
        return(
            <>

<div className="header-area" id="headerArea">
<div className="container h-100 d-flex align-items-center justify-content-between">
  {/* Logo Wrapper*/}
  <div className="logo-wrapper">
    <Link to="/">
      <img src="assets/img/core-img/logo-small.png" alt />
    </Link>
  </div>
  {/* Search Form*/}
  <div className="top-search-form">
    <form action="#" method>
      <input
        className="form-control"
        type="search"
        placeholder="Enter your keyword"
      />
      <button type="submit">
        <i className="fa fa-search" />
      </button>
    </form>
  </div>
  {/* Navbar Toggler*/}
  <div
    className="suha-navbar-toggler d-flex flex-wrap menubar"
    id="suhaNavbarToggler"
  >
    <span />
    <span />
    <span />
  </div>
</div>
</div>

</>

	
)
}

export default Search