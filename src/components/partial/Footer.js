import React, {Component} from 'react';
import {Link} from 'react-router-dom';
class Footer extends Component{
   state ={
      all:[],
    }
  async componentDidMount() {
     const url = "http://digitalsupporter.in/reactjs/allApi/contact.php";
     const response = await fetch(url);
     const data = await response.json();
    //  console.log(data[0].phone);
      // this.setState({address:data[0].address,phone:data[0].phone})
      this.setState({all:data[0]})
    //  console.log(address);
    // data.forEach(element => {
      
    // });
  }
   
      render(){
        const all=this.state.all
        console.log(55);
        console.log(all);
        return(
            <footer id="tw-footer" class="tw-footer">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-lg-4">
               <div class="tw-footer-info-box">
                  <Link to="index.html" class="footer-logo">
                     <img src="assets/images/logo/logo.png" alt="footer_logo" class="img-fluid" style={{width: '140px',height: '55px'}}/>
                  </Link>
                  <p class="footer-info-text">
                  Nos designers imaginent avec vous plusieurs versions de parcours répondant aux attentes des personnes qui utiliseront votre solution.
                     </p>
                  <div class="footer-social-link">
                     <h3>Suivez nous</h3>
                     <ul>
                     <li><a href={all.fb} target="_blank"><i class="fa fa-facebook"></i></a></li>
                     <li> <a href={all.twitter} target="_blank"><i class="fa fa-twitter"></i></a></li>
                     <li><a href={all.telegram} target="_blank"><i class="fa fa-telegram"></i></a></li>
                     </ul>
                  </div>
                  {/* End Social link */}
               </div>
               {/* End Footer info */}
               <div class="footer-awarad">
                  <img src="assets/images/icon/best.png" alt=""/>
                  <p>Meilleure entreprise de référencement 2021</p>
               </div>
            </div>
            {/* End Col */}
            <div class="col-md-12 col-lg-8">
               <div class="row">
                  <div class="col-md-6">
                     <div class="contact-us">
                        <div class="contact-icon">
                           <i class="icon icon-map2"></i>
                        </div>
                        {/* End contact Icon */}
                        <div class="contact-info">
                           <h3>##, ###</h3>
                           <p>1010 Grand Avenue</p>
                        </div>
                        {/* End Contact Info */}
                     </div>
                     {/* End Contact Us */}
                  </div>
                  {/* End Col */}
                  <div class="col-md-6">
                     <div class="contact-us contact-us-last">
                        <div class="contact-icon">
                           <i class="icon icon-phone3"></i>
                        </div>
                        {/* End contact Icon */}
                        <div class="contact-info">
                           <h3>###-###-####</h3>
                           <p>Appelez-nous</p>
                        </div>
                        {/* End Contact Info */}
                     </div>
                     {/* End Contact Us */}
                  </div>
                  {/* End Col */}
               </div>
               {/* End Contact Row */}
               <div class="row">
                  <div class="col-md-12 col-lg-6">
                     <div class="footer-widget footer-left-widget">
                        <div class="section-heading">
                           <h3>Liens utiles</h3>
                           <span class="animate-border border-black"></span>
                        </div>
                        <ul>
                           <li><Link to="#">À propos de nous</Link></li>
                           <li><Link to="#">Prestations de service</Link></li>
                           <li><Link to="#">Projets</Link></li>
                           <li><Link to="#">Notre équipe</Link></li>
                        </ul>
                        <ul>
                           <li><Link to="#">Contactez-nous</Link></li>
                           <li><Link to="#">Blog</Link></li>
                           <li><Link to="/testimonials">Témoignages</Link></li>
                           <li><Link to="#">FAQ</Link></li>
                        </ul>
                     </div>
                     {/* End Footer Widget */}
                  </div>
                  {/* End col */}
                  <div class="col-md-12 col-lg-6">
                     <div class="footer-widget">
                        <div class="section-heading">
                           <h3>Subscribe</h3>
                           <span class="animate-border border-black"></span>
                        </div>
                        <p>Ne manquez pas de vous abonner à nos nouveaux flux, veuillez remplir le formulaire ci-dessous.</p>
                        <form action="#">
                           <div class="form-row">
                              <div class="col tw-footer-form">
                                 <input type="email" class="form-control" placeholder="Email Address"/>
                                 <button type="submit"><i class="fa fa-send"></i></button>
                              </div>
                           </div>
                        </form>
                        {/* End form */}
                     </div>
                     {/* End footer widget */}
                  </div>
                  {/* End Col */}
               </div>
               {/* End Row */}
            </div>
            {/* End Col */}
         </div>
         {/* End Widget Row */}
      </div>
      {/* End Contact Container */}


      <div class="copyright">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <span>Copyright &copy; 2021, All Right Reserved.</span>
               </div>
               {/* End Col */}
               <div class="col-md-6">
                  <div class="copyright-menu">
                     <ul>
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/terms">Terms</Link></li>
                        <li><Link to="/privacy">Privacy Policy</Link></li>
                        <li><Link to="/contact">Contact</Link></li>
                     </ul>
                  </div>
               </div>
               {/* End col */}
            </div>
            {/* End Row */}
         </div>
         {/* End Copyright Container */}
      </div>
      {/* End Copyright */}
      {/* Back to top */}
      <div id="back-to-top" class="back-to-top">
         <button class="btn btn-dark" title="Back to Top">
            <i class="fa fa-angle-up"></i>
         </button>
      </div>
      {/* End Back to top */}
   </footer>
        )
    }
}
export default Footer