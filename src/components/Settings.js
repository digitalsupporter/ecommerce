import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Common from "./Common";
import Back from "./Back";
import Preloader from "./Preloader";
import Sidenav from "./Sidenav";
class Settings extends Component{
    render(){
        return(
            <>
  {/* Preloader*/}
            <Preloader/>
  {/* Internet Connection Status*/}
  <div className="internet-connection-status" id="internetStatus" />
  {/* Header Area*/}
 
  <Back
  title="Settings"
  />
  
        {/* Sidenav Black Overlay*/}
        <Sidenav/>
        <div className="page-content-wrapper">
  <div className="container">
    {/* Settings Wrapper*/}
    <div className="settings-wrapper py-3">
      {/* Single Setting Card*/}
      <div className="card settings-card">
        <div className="card-body">
          {/* Single Settings*/}
          <div className="single-settings d-flex align-items-center justify-content-between">
            <div className="title">
              <i className="lni lni-checkmark" />
              <span>Availability</span>
            </div>
            <div className="data-content">
              <div className="toggle-button-cover">
                <div className="button r">
                  <input className="checkbox" type="checkbox" defaultChecked />
                  <div className="knobs" />
                  <div className="layer" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Single Setting Card*/}
      <div className="card settings-card">
        <div className="card-body">
          {/* Single Settings*/}
          <div className="single-settings d-flex align-items-center justify-content-between">
            <div className="title">
              <i className="lni lni-alarm" />
              <span>Notifications</span>
            </div>
            <div className="data-content">
              <div className="toggle-button-cover">
                <div className="button r">
                  <input className="checkbox" type="checkbox" defaultChecked />
                  <div className="knobs" />
                  <div className="layer" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Single Setting Card*/}
      <div className="card settings-card">
        <div className="card-body">
          {/* Single Settings*/}
          <div className="single-settings d-flex align-items-center justify-content-between">
            <div className="title">
              <i className="lni lni-night" />
              <span>Night Mode</span>
            </div>
            <div className="data-content">
              <div className="toggle-button-cover">
                <div className="button r">
                  <input className="checkbox" id="darkSwitch" type="checkbox" />
                  <div className="knobs" />
                  <div className="layer" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Single Setting Card*/}
      <div className="card settings-card">
        <div className="card-body">
          {/* Single Settings*/}
          <div className="single-settings d-flex align-items-center justify-content-between">
            <div className="title">
              <i className="lni lni-world" />
              <span>Language</span>
            </div>
            <div className="data-content">
              <a href="language.html">
                English
                <i className="lni lni-chevron-right" />
              </a>
            </div>
          </div>
        </div>
      </div>
      {/* Single Setting Card*/}
      <div className="card settings-card">
        <div className="card-body">
          {/* Single Settings*/}
          <div className="single-settings d-flex align-items-center justify-content-between">
            <div className="title">
              <i className="lni lni-question-circle" />
              <span>Support</span>
            </div>
            <div className="data-content">
              <a href="support.html">
                Get Help
                <i className="lni lni-chevron-right" />
              </a>
            </div>
          </div>
        </div>
      </div>
      {/* Single Setting Card*/}
      <div className="card settings-card">
        <div className="card-body">
          {/* Single Settings*/}
          <div className="single-settings d-flex align-items-center justify-content-between">
            <div className="title">
              <i className="lni lni-protection" />
              <span>Privacy Policy</span>
            </div>
            <div className="data-content">
              <a href="privacy-policy.html">
                View
                <i className="lni lni-chevron-right" />
              </a>
            </div>
          </div>
        </div>
      </div>
      {/* Single Setting Card*/}
      <div className="card settings-card">
        <div className="card-body">
          {/* Single Settings*/}
          <div className="single-settings d-flex align-items-center justify-content-between">
            <div className="title">
              <i className="lni lni-lock" />
              <span>
                Password<span>Updated 1 month ago</span>
              </span>
            </div>
            <div className="data-content">
              <a href="change-password.html">
                Change
                <i className="lni lni-chevron-right" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


  {/* Internet Connection Status*/}
  <div className="internet-connection-status" id="internetStatus" />
  <Common/>
</>


            )
    }
}
export default Settings