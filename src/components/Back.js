import React, {Component} from 'react';

import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

const Back = (props) => {
        return(
            <>

<div className="header-area" id="headerArea">
            <div className="container h-100 d-flex align-items-center justify-content-between">
              {/* Back Button*/}
              <div className="back-button">
                <Link to="/">
                  <svg
                    className="bi bi-arrow-left"
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    fill="currentColor"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fillRule="evenodd"
                      d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
                    />
                  </svg>
                </Link>
              </div>
              {/* Page Title*/}
              <div className="page-heading">
                <h6 className="mb-0">{props.title}</h6>
              </div>
              {/* Navbar Toggler*/}
                <div
                    className="suha-navbar-toggler d-flex flex-wrap menubar"
                    id="suhaNavbarToggler"
                >
                    <span />
                    <span />
                    <span />
                </div>
            </div>
          </div>
          
</>

	
)
}

export default Back