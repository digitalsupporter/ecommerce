import React,{Component} from 'react';
 
import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Common from "./Common";

class Services extends Component{
    render(){
        return(
            <>
  <Common
    title="Services"
  />
  <section id="main-container" className="main-container">
    <div className="container">
      <div className="row">
        <div className="col-lg-3 col-md-12">
          <div className="section-heading">
            <h2>
              All <span>Services</span>
            </h2>
            <span className="animate-border tw-mt-30 tw-mb-40" />
            <p> Start working with an company that provide everything you need to anything is going very well but you dont
              know. </p>
          </div>
        </div>
        {/* Heading Col End */}
        <div className="col-lg-9 col-md-12">
          <OwlCarousel className="service-list-carousel owl-carousel"
          items={3}
   loop={true}
   nav={false}
   autoplay={true}
   dots={true}
   
   navText={['<i class="fas fa-arrow-left"></i>','<i class="fas fa-arrow-right"></i>']}
          >
            <div className="tw-service-box-list text-center">
              <div className="service-list-bg service-list-bg-1 d-table">
                <div className="service-list-icon d-table-cell">
                  <img src="assets/images/icon/service1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* List Bg End */}
              <h3>Search Strategy</h3>
              <p>Start working with an company that provide everything you</p>
            </div>
            {/* List Box End */}
            <div className="tw-service-box-list text-center">
              <div className="service-list-bg service-list-bg-2 d-table">
                <div className="service-list-icon d-table-cell">
                  <img src="assets/images/icon/service2.png" alt className="img-fluid" />
                </div>
              </div>
              {/* List Bg End */}
              <h3>Search Strategy</h3>
              <p>Start working with an company that provide everything you</p>
            </div>
            {/* List Box End */}
            <div className="tw-service-box-list text-center">
              <div className="service-list-bg service-list-bg-3 d-table">
                <div className="service-list-icon d-table-cell">
                  <img src="assets/images/icon/service3.png" alt className="img-fluid" />
                </div>
              </div>
              {/* List Bg End */}
              <h3>Search Strategy</h3>
              <p>Start working with an company that provide everything you</p>
            </div>
            {/* List Box End */}
            <div className="tw-service-box-list text-center">
              <div className="service-list-bg service-list-bg-1 d-table">
                <div className="service-list-icon d-table-cell">
                  <img src="assets/images/icon/service1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* List Bg End */}
              <h3>Search Strategy</h3>
              <p>Start working with an company that provide everything you</p>
            </div>
            {/* List Box End */}
          </OwlCarousel>
          {/* Carousel End */}
        </div>
        {/* Content Col end */}
      </div>
      {/* Row End */}
    </div>
    {/* Container End */}
  </section>
  {/* Service List End */}
  <section id="tw-analysis" className="tw-analysis-area">
    <div className="analysis-bg-pattern d-none d-md-inline-block">
      <img className="wow fadeInUp" src="assets/images/check-seo/cloud.png" alt />
      <img className="wow fadeInUp" src="assets/images/check-seo/cloud2.png" alt />
      <img className="wow fadeInUp" src="assets/images/check-seo/announce.png" alt />
      <img className="wow fadeInUp" src="assets/images/check-seo/chart.png" alt />
    </div>
    {/* End Analysis Pattern img */}
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-8 text-center wow fadeInDown">
          <h2 className="column-title">
            Check your
            <span className="text-white">Website’s SEO</span>
          </h2>
          <div className="analysis-form">
            <form className="form-vertical">
              <div className="row justify-content-center">
                <div className="col-lg-4 col-md-12 no-padding">
                  <div className="form-group tw-form-round-shape">
                    <input type="url" id="url" name="url" placeholder="Type website URL" className="form-control" />
                  </div>
                </div>
                <div className="col-lg-4 col-md-12 no-padding">
                  <div className="form-group tw-form-round-shape">
                    <input type="email" id="email" name="email" placeholder="Your Email" className="form-control" />
                  </div>
                </div>
                <div className="col-lg-4 col-md-12 no-padding">
                  <div className="form-group">
                    <input type="submit" defaultValue="submit now" />
                  </div>
                </div>
              </div>
            </form>
            {/* End Form */}
          </div>
          {/* End Analysis form */}
        </div>
        {/* Col End */}
      </div>
      {/* End Row */}
    </div>
    {/* End container */}
  </section>
  {/* End Analysis Section */}
  <section id="tw-service-features" className="tw-service-features">
    <div className="container">
      <div className="row">
        <div className="col text-center">
          <div className="section-heading">
            <h2>
              <small>Our Features</small>
              Our Best <span>Features</span>
            </h2>
            <span className="animate-border ml-auto mr-auto tw-mt-20 tw-mb-40" />
          </div>
          {/* Heading End */}
        </div>
        {/* Col End */}
      </div>
      {/* Heading Row End */}
      <div className="row">
        <div className="col-md-4 text-center">
          <div className="tw-service-features-box">
            <div className="service-feature-icon-bg service-bg-icon-1">
              <div className="service-feature-icon">
                <img src="assets/images/icon/service1.png" alt />
              </div>
            </div>
            <h3>Social Marketing</h3>
            <p>Start working with an company that provide everything you need to any</p>
            <a href="#" className="tw-readmore">Read More</a>
          </div>
          {/* Features box End */}
        </div>
        {/* Col End */}
        <div className="col-md-4 text-center">
          <div className="tw-service-features-box">
            <div className="service-feature-icon-bg service-bg-icon-1">
              <div className="service-feature-icon">
                <img src="assets/images/icon/service2.png" alt />
              </div>
            </div>
            <h3>Mobile Optimization</h3>
            <p>Start working with an company that provide everything you need to any</p>
            <a href="#" className="tw-readmore">Read More</a>
          </div>
          {/* Features box End */}
        </div>
        {/* Col End */}
        <div className="col-md-4 text-center">
          <div className="tw-service-features-box">
            <div className="service-feature-icon-bg service-bg-icon-1">
              <div className="service-feature-icon">
                <img src="assets/images/icon/service3.png" alt />
              </div>
            </div>
            <h3>Reputation Management</h3>
            <p>Start working with an company that provide everything you need to any</p>
            <a href="#" className="tw-readmore">Read More</a>
          </div>
          {/* Features box End */}
        </div>
        {/* Col End */}
      </div>
      {/* Content Row End */}
    </div>
    {/* Container End */}
  </section>
  {/* Service Features End */}
  <section id="tw-testimonial" className="tw-testimonial">
    <div className="container">
      <div className="row align-items-center justify-content-center">
        <div className="col-md-6 wow fadeInLeft" data-wow-duration="1s">
          <div className="tw-testimonial-content text-center">
            <h2 className="section-title">Love From Clients</h2>
            <span className="animate-border border-green tw-mt-20 tw-mb-40 ml-auto mr-auto" />
          </div>
          <OwlCarousel className="tw-testimonial-carousel owl-carousel"
          items={1}
   loop={true}
   nav={true}
   autoplay={true}
   dots={true}
   navText={['<i class="icon icon-arrow-left"></i>','<i class="icon icon-right-arrow"></i>']}
          >
            <div className="tw-testimonial-wrapper">
              <div className="testimonial-bg testimonial-bg-orange">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-text">
                <p>Start working with an company that can do provide every thing at you need to generate awareness,
                  drive traffic, connect with</p>
                <div className="testimonial-meta">
                  <h4>
                    Jason Stattham
                    <small>CEO Microhost</small>
                  </h4>
                  <i className="icon icon-quote2" />
                </div>
                {/* End Testimonial Meta */}
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
            <div className="tw-testimonial-wrapper">
              <div className="testimonial-bg testimonial-bg-orange">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-text">
                <p>Start working with an company that can do provide every thing at you need to generate awareness,
                  drive traffic, connect with</p>
                <div className="testimonial-meta">
                  <h4>
                    Jason Stattham
                    <small>CEO Microhost</small>
                  </h4>
                  <i className="icon icon-quote2" />
                </div>
                {/* End Testimonial meta */}
              </div>
              {/* End Testimonial Text */}
            </div>
            {/* End Tw testimonial wrapper */}
            <div className="tw-testimonial-wrapper">
              <div className="testimonial-bg testimonial-bg-orange">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* end testimonial bg */}
              <div className="testimonial-text">
                <p>Start working with an company that can do provide every thing at you need to generate awareness,
                  drive traffic, connect with</p>
                <div className="testimonial-meta">
                  <h4>
                    Jason Stattham
                    <small>CEO Microhost</small>
                  </h4>
                  <i className="icon icon-quote2" />
                </div>
                {/* End testimonial meta */}
              </div>
              {/* End testimonial text */}
            </div>
            {/* End tw testimonial wrapper */}
          </OwlCarousel>
          {/* End Tw testimonial carousel */}
        </div>
        {/* End Col */}
      </div>
      {/* End Row */}
    </div>
    {/* End Container */}
  </section>
  {/* End TW testimonial */}
  <section id="tw-features-contact" className="tw-features-contact">
    <div className="container">
      <div className="row">
        <div className="col-md-6">
          <div className="section-heading">
            <h2>Feel free to contact us</h2>
            <span className="animate-border tw-mt-30 tw-mb-40" />
          </div>
          <div className="tw-features-contact-info">
            <p>Start working with an company that can provide every thing you need to generate awareness, drive traffic
              connect with customers, and increase sales nascetur</p>
            <div className="row">
              <div className="col-lg-6 col-md-12">
                <div className="contact-us">
                  <div className="contact-icon">
                    <i className="icon icon-phone3" />
                  </div>
                  {/* End contact Icon */}
                  <div className="contact-info">
                    <h3>009-215-5596</h3>
                    <p>Give us a call</p>
                  </div>
                  {/* End Contact Info */}
                </div>
                {/* End Contact Us */}
              </div>
              <div className="col-lg-6 col-md-12">
                <div className="contact-us">
                  <div className="contact-icon">
                    <i className="icon icon-envelope2" />
                  </div>
                  {/* End contact Icon */}
                  <div className="contact-info">
                    <h3>mail@example.com</h3>
                    <p>24/7 online support</p>
                  </div>
                  {/* End Contact Info */}
                </div>
                {/* End Contact Us */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="contact-us-form service-contact-form">
            <form id="contact-form" className="contact-form" action="http://demo.themewinter.com/html/seobin/contact-form.php" method="POST">
              <div className="error-container" />
              <div className="row">
                <div className="col-lg-6">
                  <div className="form-group">
                    <input className="form-control form-name" name="name" id="name" placeholder="Name" type="text" required />
                  </div>
                </div>
                {/* Col end */}
                <div className="col-lg-6">
                  <div className="form-group">
                    <input className="form-control form-email" name="email" placeholder="Email" type="email" required />
                  </div>
                </div>
                {/* Col End */}
                <div className="col-lg-12">
                  <div className="form-group">
                    <input className="form-control form-subject" placeholder="Subject" name="subject" id="subject" type="text" />
                  </div>
                </div>
                {/* Col End */}
                <div className="col-lg-12">
                  <div className="form-group">
                    <textarea className="form-control form-message required-field" id="message" placeholder="Message" rows={1} defaultValue={""} />
                  </div>
                </div>
                {/* Col 12 end */}
              </div>
              {/* Form row end */}
              <div className="text-right">
                <button className="btn btn-primary tw-mt-30" type="submit">Contact US</button>
              </div>
            </form>
            {/* Form end */}
          </div>
          {/* Contact us form end */}
        </div>
        {/* Col End */}
      </div>
      {/* Row End */}
    </div>
    {/* Container End */}
  </section>
  {/* Feature Contact Us end */}
</>

         )
     }
 }
 export default Services