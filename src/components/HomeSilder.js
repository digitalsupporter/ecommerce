import React, {Component, useEffect} from 'react';

import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
const HomeSilder = (props) => {
    return(
        <>
<div className="container">
      <div className="pt-3">
        {/* Hero Slides*/}
        <OwlCarousel className="hero-slides owl-carousel"
        items={1}
        loop={true}
        nav={true}
        autoplay={true}
        dots={true}
        navText={['<i className="icon icon-left-arrow2"></i>','<i className="icon icon-right-arrow2"></i>']}
        >

          {/* Single Hero Slide*/}
          <div
            className="single-hero-slide"
            style={{ backgroundImage: 'url("assets/img/bg-img/1.jpg")' }}
          >
            <div className="slide-content h-100 d-flex align-items-center">
              <div className="slide-text">
                <h4
                  className="text-white mb-0"
                  data-animation="fadeInUp"
                  data-delay="100ms"
                  data-duration="1000ms"
                >
                  Amazon Echo
                </h4>
                <p
                  className="text-white"
                  data-animation="fadeInUp"
                  data-delay="400ms"
                  data-duration="1000ms"
                >
                  3rd Generation, Charcoal
                </p>
                <Link
                  className="btn btn-primary btn-sm"
                  to="#"
                  data-animation="fadeInUp"
                  data-delay="800ms"
                  data-duration="1000ms"
                >
                  Buy Now
                </Link>
              </div>
            </div>
          </div>
          {/* Single Hero Slide*/}
          <div
            className="single-hero-slide"
            style={{ backgroundImage: 'url("assets/img/bg-img/2.jpg")' }}
          >
            <div className="slide-content h-100 d-flex align-items-center">
              <div className="slide-text">
                <h4
                  className="text-white mb-0"
                  data-animation="fadeInUp"
                  data-delay="100ms"
                  data-duration="1000ms"
                >
                  Light Candle
                </h4>
                <p
                  className="text-white"
                  data-animation="fadeInUp"
                  data-delay="400ms"
                  data-duration="1000ms"
                >
                  Now only $22
                </p>
                <Link
                  className="btn btn-success btn-sm"
                  to="#"
                  data-animation="fadeInUp"
                  data-delay="500ms"
                  data-duration="1000ms"
                >
                  Buy Now
                </Link>
              </div>
            </div>
          </div>
          {/* Single Hero Slide*/}
          <div
            className="single-hero-slide"
            style={{ backgroundImage: 'url("assets/img/bg-img/3.jpg")' }}
          >
            <div className="slide-content h-100 d-flex align-items-center">
              <div className="slide-text">
                <h4
                  className="text-white mb-0"
                  data-animation="fadeInUp"
                  data-delay="100ms"
                  data-duration="1000ms"
                >
                  Best Furniture
                </h4>
                <p
                  className="text-white"
                  data-animation="fadeInUp"
                  data-delay="400ms"
                  data-duration="1000ms"
                >
                  3 years warranty
                </p>
                <Link
                  className="btn btn-danger btn-sm"
                  to="#"
                  data-animation="fadeInUp"
                  data-delay="800ms"
                  data-duration="1000ms"
                >
                  Buy Now
                </Link>
              </div>
            </div>
          </div>
        </OwlCarousel>
      </div>
    </div>
    
    </>

	
    )
}

export default HomeSilder