import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import Common from "./Common";
class Career extends Component{
    render(){
        return(
			<>
            <Common
    title="Career"
  />
  <div className="career" style={{width: '100%', height: 300, paddingTop: 100, paddingBottom: 100}}>
    <div className="container" style={{height: 100}}>
      <div className="row" style={{marginLeft: '-15px', marginRight: '-15px'}}>
        <div className="col-lg-12" style={{textAlign: 'center'}}>
          <h1 style={{marginBottom: 10, fontSize: 40, fontWeight: 400, color: '#222222', lineHeight: '1.1'}}>Why Join us?</h1>
          <p>
            At Raz, we love working with the best and brightest. Our collaborative work environment and innovative clients create an atmosphere that is challenging and rewarding.
          </p>
        </div>
      </div>
    </div>
  </div>
  <div className="about" style={{paddingBottom: 100, paddingTop: 100, backgroundColor: '#f5f5f5', height: 'auto'}}>
    <div className="container">
      <h1 style={{textAlign: 'center', marginBottom: 10, fontSize: 40, fontWeight: 400, color: '#222222', lineHeight: '1.1'}}>Open Positions</h1>
      <div className="row">
        <div className="col-md-12">
          <ul className="nav nav-tab" style={{justifyContent: 'center', borderBottom: '1px solid #ddd', padding: '10px 25px', fontSize: 14, lineHeight: 26, textTransform: 'uppercase'}}>
            <li className="nav-item text-center" style={{padding: '10px 25px'}}>
              <a className="nav-link active" href="#" style={{color: '#495057'}}>
                Yangon, Myanmar
              </a>
            </li>
            <li className="nav-item text-center" style={{padding: '10px 25px'}}>
              <a className="nav-link" href="#">
                <h5 style={{fontWeight: 400, color: '#495057'}}>Bangalore, India</h5>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div className="row" style={{paddingTop: 45, paddingBottom: 100}}>
        <div className="col-md-6" style={{paddingRight: 15, paddingLeft: 15, position: 'relative'}}>
          <div className="card" style={{border: '1px solid #e5e5e5', padding: 40, lineHeight: 40}}>
            <h2 style={{fontSize: 30, lineHeight: '1.1', fontWeight: 400, color: '#222222'}}>Php Developer</h2>
            <p style={{color: '#848484', fontWeight: 400}}>
              2 - 5 yrs Experience Php, Codigniter, Wordpress, Moodle, Laravel, MySQL, HTML5, API
            </p>
            <a href="#" className="btn btn-secondary-outlined" style={{color: '#848484', borderRadius: 25, height: 40, width: 150, lineHeight: 12, border: '1px solid #dedede'}}>
              Apply Now
            </a>
          </div>
        </div>
        <div className="col-md-6" style={{paddingRight: 15, paddingLeft: 15, position: 'relative'}}>
          <div className="card" style={{border: '1px solid #e5e5e5', padding: 40}}>
            <h2 style={{fontSize: 30, lineHeight: '1.1', fontWeight: 400, color: '#222222'}}>Front-End Developer</h2>
            <p style={{color: '#848484', fontWeight: 400}}>4+ yrs Experience jQuery, Javascript &amp; HTML/CSS
            </p>
            <a href="#" className="btn btn-secondary-outlined" style={{color: '#848484', borderRadius: 25, height: 40, width: 150, lineHeight: 12, border: '1px solid #dedede'}}>
              Apply Now
            </a>
          </div>
        </div>
      </div>
    </div>
    <section className="getin-touch" style={{padding: '100px 0px', height: 250, backgroundImage: 'url("image/1-row.jpg")', backgroundSize: 'cover', backgroundPosition: 'center'}}>
      <div className="container" style={{paddingRight: 15, paddingLeft: 15}}>
        <div className="row">
          <div className="col-md-8 col-12">
            <h4 style={{lineHeight: 48, color: '#fff', fontSize: 24, fontWeight: 500}}>Get in Touch to start a conversation about your project</h4>
          </div>
          <div className="col-md-4 col-12">
            <a href="#" className="btn " style={{borderRadius: 25, height: 50, lineHeight: 50, padding: '0 30px', backgroundColor: '#4ac8ed', border: '1px solid #4ac8ed', color: 'white'}}>GET IN TOUCH</a>
          </div>
        </div>
      </div>
    </section>
    <section className="newsletter" style={{paddingTop: 100, paddingBottom: 100, padding: '80px 0'}}>
      <div className="container" style={{}}>
        <div className="row" style={{marginLeft: '-15px', marginRight: '-15px'}}>
          <div className="col-lg-12">
            <h4 style={{marginBottom: 30, fontSize: 24, textAlign: 'center', fontWeight: 400, color: '#222222'}}>Signup to receive 
              <span className="text-primary">free</span>
              insights on product development &amp; innovation
            </h4>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12 col-12">
            <div className="col-lg-8" style={{position: 'relative', right: '-290px'}}>
              <input type="email" className="form-control" placeholder="Your email" style={{height: '3rem', width: 400, borderRadius: 25, fontSize: 14, boxShadow: '2px 3px 9px rgb(44 40 86 /10%)'}} />
            </div>
            <div className="col-lg-4" style={{position: 'relative', top: '-49px', left: '-21px', float: 'right'}}>
              <button className="btn btn-primary" style={{borderRadius: 25, height: 50, borderColor: '#4ac8ed', padding: '0 30px', backgroundColor: '#4ac8ed'}}>SUBSCRIBE</button>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</>

        )
    }
}
export default Career