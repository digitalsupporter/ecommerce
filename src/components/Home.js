import React,{Component} from 'react';
import {Redirect, Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Common from "./Common";
import AddToHomeScreen from "./AddToHomeScreen";
import HomeSilder from "./HomeSilder";
import ProductCat from "./ProductCat";
import CoolBanner from "./CoolBanner";
import BestSale from "./BestSale";
import Discount from "./Discount";
import Flash from "./Flash";
import Featured from "./Featured";
import Search from "./Search";
import Preloader from "./Preloader";
import Sidenav from "./Sidenav";
// import Css from './assets/css/iconfonts.css';
import { withCookies } from "react-cookie";
//Owl Carousel Settings

class Home extends  Component{
  state = {
    all:[]
  };
  
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick;
    this.user = this.user;
  }
  user =()=>{
    const user = this.props.cookies.get("user");
    if(user){
       console.log(user);
       
    }else{
      return <Redirect to='/login'/>
    }
   }
  handleClick=(e)=> {
    // console.log(e.target.id);
    console.log(e.currentTarget.id);
    let id = e.currentTarget.id;
    let item;
    const user = this.props.cookies.get("user");
    item='1';
    // alert("Fetch Api code here");
    var InsertAPIURL="https://digitalsupporter.in/reactjs/allApi/seo/addtocard.php";
    var headers={
        'Accept':'application/json',
        'Contect-Type':'application/json'
   };
   var Data={
       product_id:id,
       user:user,
       item:item,
       
   };
   fetch(InsertAPIURL,
       {
           method:'POST',
           headers: headers,
           body:JSON.stringify(Data)
       }
   )
   .then((response)=>response.json())
   .then((response)=>
   {
     console.log(response);
    let cartval = document.getElementById("cartval");
    cartval.innerHTML = response.cart;
   })
   .catch((error)=>
   {
       alert("Error"+error);
       console.log(error);
       this.setState({authError: true, isLoading: false});
       
   })
  }
  async componentDidMount() {
    const url = "http://digitalsupporter.in/reactjs/allApi/seo/all_product.php";
    const response = await fetch(url);
    const data = await response.json();
    console.log((data[0][0].price_after));
     // this.setState({address:data[0].address,phone:data[0].phone})
     this.setState({all:data[0]})
   //  console.log(address);
   // data.forEach(element => {
     
   // });
   
 }

    render(){
      const all=this.state.all
      console.log(55);
      console.log(all);
        return(
          
         <div>
            <>
            {this.user()}
  {/* Preloader*/}
  <Preloader/>
  {/* Header Area*/}
 
        <Search/>
        
  {/* Sidenav Black Overlay*/}
          <Sidenav/>
  {/* PWA Install Alert*/}

  <AddToHomeScreen/>
  <div className="page-content-wrapper">
    <HomeSilder/>
    {/* Product Catagories*/}
   <ProductCat/>
    {/* Flash Sale Slide*/}
    <Flash/>
    {/* Top Products*/}
    <div className="top-products-area clearfix py-3">
      <div className="container">
        <div className="section-heading d-flex align-items-center justify-content-between">
          <h6>Top Products</h6>
          <Link className="btn btn-danger btn-sm" to="shop-grid.html">
            View All
          </Link>
        </div>
        <div className="row g-3">
          {/* Single Top Product Card*/}
          {
        all.map((all , index)=>
        <>
          <div className="col-6 col-md-4 col-lg-3" key={all.id}>
            <div className="card top-product-card" >
              <div className="card-body">
                <span className="badge badge-success">Sale</span>
                <Link className="wishlist-btn" to="#">
                  <i className="lni lni-heart" />
                </Link>
                <Link
                  className="product-thumbnail d-block"
                  to={"/single"+all.id}
                >
                  <img className="mb-2" src={all.image} alt />
                </Link>
                <Link className="product-title d-block" to={"/single"+all.id}>
                {all.product_name}
                </Link>
                <p className="sale-price">
                  ${all.price_after}<span>${all.price_before}</span>
                </p>
                <div className="product-rating">
                  <i className="lni lni-star-filled" />
                  <i className="lni lni-star-filled" />
                  <i className="lni lni-star-filled" />
                  <i className="lni lni-star-filled" />
                  <i className="lni lni-star-filled" />
                </div>
                <button className="btn btn-success btn-sm" id={all.product_id} onClick={this.handleClick} >
                  <i className="lni lni-plus" />
                </button>
              </div>
            </div>
          </div>
          {/* Single Top Product Card*/}
          </>
        )
        }
        </div>
      </div>
    </div>
    {/* Cool Facts Area*/}
    <CoolBanner/>
    {/* Weekly Best Sellers*/}
    <BestSale/>
    {/* Discount Coupon Card*/}
    <Discount/>
    {/* Featured Products Wrapper*/}
    <Featured/>
    </div>
  {/* Internet Connection Status*/}
  <div className="internet-connection-status" id="internetStatus" />
  {/* Footer Nav*/}
          <Common/>
</>

         </div>
          )
    }
}
export default withCookies(Home)