import React,{Component} from 'react';
import {Redirect,Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Common from "./Common";
import Back from "./Back";
import Preloader from "./Preloader";
import Sidenav from "./Sidenav";
import { withCookies } from "react-cookie";
class Cart extends Component{
  state = {
    all:[]
  };
  constructor(props) {
    super(props);
    this.handleClickAdd = this.handleClickAdd;
    this.user = this.user;
  }
  user =()=>{
    const user = this.props.cookies.get("user");
    if(user){
       console.log(user);
       
    }else{
      return <Redirect to='/login'/>
    }
   }
  handleClickAdd=(e)=> {
    // console.log(e.target.id);
    console.log(e.currentTarget.id);
    let id = e.currentTarget.id;
    let item;
    const user = this.props.cookies.get("user");
    item='1';
    // alert("Fetch Api code here");
    var InsertAPIURL="https://digitalsupporter.in/reactjs/allApi/seo/addtocard.php";
    var headers={
        'Accept':'application/json',
        'Contect-Type':'application/json'
   };
   var Data={
       product_id:id,
       user:user,
       item:item,
       
   };
   fetch(InsertAPIURL,
       {
           method:'POST',
           headers: headers,
           body:JSON.stringify(Data)
       }
   )
   .then((response)=>response.json())
   .then((response)=>
   {
     console.log(response);
    let cartval = document.getElementById("cartval");
    cartval.innerHTML = response.cart;
    
   })
   .catch((error)=>
   {
       alert("Error"+error);
       console.log(error);
       this.setState({authError: true, isLoading: true});
       
   })
  }

  async componentDidMount() {
    
    const user = this.props.cookies.get("user");
    var headers={'Accept':'application/json','Contect-Type':'application/json'};
  var Data={user:user};
    const url = "https://digitalsupporter.in/reactjs/allApi/seo/all_cart.php";
    const response = await fetch(url,
      {method:'POST',headers: headers,body:JSON.stringify(Data)}
      );
    const data = await response.json();
     console.log(data)
     // this.setState({address:data[0].address,phone:data[0].phone})
     this.setState({all:data})
   //  console.log(address);
   // data.forEach(element => {
     
   // });
   
 }
    render(){
      const all=this.state.all
      console.log(55);
      console.log(all);
        return(
            <>
  {/* Preloader*/}
            <Preloader/>
  {/* Internet Connection Status*/}
  <div className="internet-connection-status" id="internetStatus" />
  {/* Header Area*/}
 
  <Back
  title="Live Chat"
  />
  
        {/* Sidenav Black Overlay*/}
        <Sidenav/>
                
  <div className="page-content-wrapper">
    <div className="container">
      {/* Cart Wrapper*/}
      <div className="cart-wrapper-area py-3">
        <div className="cart-table card mb-3">
          <div className="table-responsive card-body">
            <table className="table mb-0">
              <tbody>
                  {
                    all.map((all , index)=>
                    <>
                <tr key={all.id}>
                  <th scope="row">
                    <a className="remove-product" href="#">
                      <i className="lni lni-close" />
                    </a>
                  </th>
                  <td>
                    <img src={all.image} alt />
                  </td>
                  <td>
                    <a href="single-product.html">
                      Ciramic Pot Set<span>${all.price_after} × {all.item}</span>
                    </a>
                  </td>
                  <td>
                  <button className="btn btn-success btn-sm" id={all.product_id} onClick={this.handleClick} style={{marginBottom:"5px", width:"40px"}}>
                        <i className="lni lni-minus" />
                      </button>
                    <div className="quantity">
                    
                      <input
                        className="qty-text"
                        type="text"
                        defaultValue={all.item}
                      />
                    </div>
                    
                    <button className="btn btn-success btn-sm" id={all.product_id} onClick={this.handleClickAdd} style={{marginTop:"5px", width:"40px"}}>
                        <i className="lni lni-plus" />
                      </button>
                  </td>
                </tr>
                </>
                  )
                  }
              </tbody>
            </table>
          </div>
        </div>
        {/* Coupon Area*/}
        <div className="card coupon-card mb-3">
          <div className="card-body">
            <div className="apply-coupon">
              <h6 className="mb-0">Have a coupon?</h6>
              <p className="mb-2">
                Enter your coupon code here &amp; get awesome discounts!
              </p>
              <div className="coupon-form">
                <form action="#">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="SUHA30"
                  />
                  <button className="btn btn-primary" type="submit">
                    Apply
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
        {/* Cart Amount Area*/}
        <div className="card cart-amount-area">
          <div className="card-body d-flex align-items-center justify-content-between">
            <h5 className="total-price mb-0">
              $<span className="counter">38.84</span>
            </h5>
            <a className="btn btn-warning" href="checkout.html">
              Checkout Now
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/* Internet Connection Status*/}
  <div className="internet-connection-status" id="internetStatus" />
  <Common/>
</>


            )
    }
}
export default withCookies(Cart)