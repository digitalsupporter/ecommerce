import React,{Component} from 'react';
import {Redirect, Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Common from "./Common";
import Back from "./Back";
import Preloader from "./Preloader";
import Sidenav from "./Sidenav";
import { instanceOf } from "prop-types";
import { withCookies, Cookies } from "react-cookie";
class Login extends Component{


  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  state = {
    user: this.props.cookies.get("user") || "",
    redirect: false,
    authError: false,
    isLoading: false,
    // location: {},
  };

    constructor(props)
    {
        super(props);
        this.state={email:'',message:''};
        this.emailval = this.emailval.bind(this);
        this.passwordval = this.passwordval.bind(this);
    }


    emailval(event) {
      this.setState({email: event.target.value});
    }

    passwordval(event) {
      this.setState({password: event.target.value});
    }

    InsertRecord=()=>
    {
      var email = this.state.email;
      var password = this.state.password;
      const { cookies } = this.props;
        if(email.length==0 || password.length==0)
        {
            alert("Required Field is Missing");
        }
        else
        {
            // alert("Fetch Api code here");
            var InsertAPIURL="https://digitalsupporter.in/reactjs/allApi/seo/login.php";

             var headers={
                 'Accept':'application/json',
                 'Contect-Type':'application/json'
            };
            var Data={
                email:email,
                password:password
                
            };
             console.log(Data);
            fetch(InsertAPIURL,
                {
                    method:'POST',
                    headers: headers,
                    body:JSON.stringify(Data)
                    
                }
            )
            .then((response)=>response.json())
            .then((response)=>
            {
              if(response.status==1){
                
              cookies.set("user", email, { path: "/" }); // setting the cookie
              this.setState({ user: cookies.get("user") });
            this.setState({redirect: true, isLoading: false});
              }else{
                alert('wrong email and password');
              }
                   

            })
            .catch((error)=>
            {
                alert("Error"+error);
                console.log(error);
                this.setState({authError: true, isLoading: false});
            })
        }
    }
    renderRedirect = () => {
      if (this.state.redirect) {
          return <Redirect to='/'/>
      }
  };

    render(){
      const { user } = this.state;
        return(
            <>
  {/* Preloader*/}
            <Preloader/>
  {/* Internet Connection Status*/}
  <div className="internet-connection-status" id="internetStatus" />
  {/* Header Area*/}
  <div className="login-wrapper d-flex align-items-center justify-content-center text-center">
  {/* Background Shape*/}
  <div className="background-shape" />
  <div className="container">
    <div className="row justify-content-center">
      <div className="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5">
        <img className="big-logo" src="img/core-img/logo-white.png" alt />
        {/* Register Form*/}
        <div className="register-form mt-5 px-4">
            
            <div className="form-group text-start mb-4">
              <span>Email</span>
              <label htmlFor="email">
                <i className="lni lni-user" />
              </label>
              <input
                className="form-control"
                id="email"
                type="text"
                placeholder="Enter Email"
                value={this.state.email} onChange={this.emailval}
              />
            </div>
            <div className="form-group text-start mb-4">
              <span>Password</span>
              <label htmlFor="password">
                <i className="lni lni-lock" />
              </label>
              <input
                className="form-control"
                id="password"
                type="password"
                placeholder="Enter Password"
                value={this.state.password} onChange={this.passwordval}
              />
            </div>
            <button className="btn btn-success btn-lg w-100" onClick={this.InsertRecord} >
              Log In
            </button>
            {user && <p>{user}</p>}
        </div>
        {/* Login Meta*/}
        <div className="login-meta-data">
          <a
            className="forgot-password d-block mt-3 mb-1"
            href="forget-password.html"
          >
            Forgot Password?
          </a>
          <p className="mb-0">
            Didn't have an account?
            <a className="ms-1" href="register.html">
              Register Now
            </a>
          </p>
        </div>
        {/* View As Guest*/}
        <div className="view-as-guest mt-3">
          <a className="btn" href="home.html">
            View as Guest
          </a>
        </div>
      </div>
    </div>
  </div>
  {this.renderRedirect()}
</div>

</>

            )
    }
}
export default withCookies(Login)