import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import Common from "./Common";
import RightSide from "./RightSide";
class AllBlog extends Component{
  state ={
    all:[],
  }
async componentDidMount() {
   const url = "http://digitalsupporter.in/reactjs/allApi/seoblog.php";
   const response = await fetch(url);
   const data = await response.json();
  //  console.log(data[0].phone);
    // this.setState({address:data[0].address,phone:data[0].phone})
    this.setState({all:data})
  //  console.log(address);
  // data.forEach(element => {
    
  // });
}
 
    render(){
      const all=this.state.all
      console.log(55);
      console.log(all);
        return(
      <>
            <Common
    title="Notre blog"
  />
   <section id="main-container" className="main-container">
    <div className="container">
      <div className="row">
      
        <div className="col-lg-8 col-md-12" >
        {
        all.map((all , index)=><>
      
          <article className="post tw-news-post" key={all.id}>
            <div className="post-media post-image">
              <img src="assets/images/news/post1.jpg" className="img-fluid" alt />
            </div>
            {/* End Post Media */}
            <div className="post-body">
              <div className="post-item-date">
                <div className="post-date">
                  <span className="date">{all.date}</span>
                  <span className="month">May</span>
                </div>
              </div>
              {/* End Post Item Date */}
              <div className="entry-header">
                <div className="post-meta">
                  <span className="post-author">
                    Posté par<a href="#"> {all.added_by}</a>
                  </span>
                  <span className="post-cat">
                    <i className="icon icon-folder" /><a href="#"> {all.category}</a>
                  </span>
                  <span className="post-comment"><i className="icon icon-comment" />5 <a href="#" className="comments-link">Catégories</a></span>
                </div>
                <h2 className="entry-title">
                  <a href="#">{all.heading}</a>
                </h2>
                {/* End Post Meta */}
              </div>
              {/* header end */}
              <div className="entry-content">
                <p>
                  {all.short_description}
                </p>
              </div>  
              {/* End Entry Content */}
              <div className="post-footer">
                <Link to={"newsSingle"+all.id} className="btn btn-dark">Continuer <i className="icon icon-arrow-right" /></Link>
              </div>
              {/* End Post Footer */}
            </div>
            {/* End Post Body */}
          </article>
          </>
        )
        }
          {/* 1st article end end */}
          <article className="post tw-news-post">
            <div className="post-media post-video">
              <img src="assets/images/news/post2.jpg" className="img-fluid" alt />
              <a className="video-popup" href="http://www.youtube.com/watch?v=0O2aH4XLbto">
                <div className="video-icon">
                  <i className="icon icon-play" />
                </div>
              </a>
            </div>
            {/* End Post Media */}
            <div className="post-body">
              <div className="post-item-date">
                <div className="post-date">
                  <span className="date">29</span>
                  <span className="month">May</span>
                </div>
              </div>
              {/* End Post Item Date */}
              <div className="entry-header">
                <div className="post-meta">
                  <span className="post-author">
                    Posté par<a href="#"> Administrateur</a>
                  </span>
                  <span className="post-cat">
                    <i className="icon icon-folder" /><a href="#"> SEO, Marketing</a>
                  </span>
                  <span className="post-comment"><i className="icon icon-comment" />5 <a href="#" className="comments-link">Catégories</a></span>
                </div>
                <h2 className="entry-title">
                  <a href="#">Google web ranking changing much previous month result</a>
                </h2>
                {/* End Post Meta */}
              </div>
              {/* header end */}
              <div className="entry-content">
                <p>
                  Financial engagements are typically multifaceted, solving for specific of digital marketing answer moving challenges while
                  buildings ongoing client capabilities. Into addition into defining new roles but helping develop
                  employees’ skills
                </p>
              </div>
              {/* End Entry Content */}
              <div className="post-footer">
                <Link to="newsSingle" className="btn btn-dark">Continuer <i className="icon icon-arrow-right" /></Link>
              </div>
              {/* End Post Footer */}
            </div>
            {/* End Post Body */}
          </article>
          {/* 2nd article end end */}
          <article className="post tw-news-post">
            <div className="post-body">
              <div className="post-item-date">
                <div className="post-date">
                  <span className="date">29</span>
                  <span className="month">May</span>
                </div>
              </div>
              {/* End Post Item Date */}
              <div className="entry-header">
                <div className="post-meta">
                  <span className="post-author">
                    Posté par<a href="#"> Administrateur</a>
                  </span>
                  <span className="post-cat">
                    <i className="icon icon-folder" /><a href="#"> SEO, Marketing</a>
                  </span>
                  <span className="post-comment"><i className="icon icon-comment" />5 <a href="#" className="comments-link">Catégories</a></span>
                </div>
                <h2 className="entry-title">
                  <a href="#">Google web ranking changing much previous month result</a>
                </h2>
                {/* End Post Meta */}
              </div>
              {/* header end */}
              <div className="entry-content">
                <p>
                  Financial engagements are typically multifaceted, solving for specific of digital marketing answer moving challenges while
                  buildings ongoing client capabilities. Into addition into defining new roles but helping develop
                  employees’ skills
                </p>
              </div>
              {/* End Entry Content */}
              <div className="post-footer">
                <Link to="newsSingle" className="btn btn-dark">Continuer <i className="icon icon-arrow-right" /></Link>
              </div>
              {/* End Post Footer */}
            </div>
            {/* End Post Body */}
          </article>
          {/* 3rd article end end */}
          <div className="paging text-center">
            <ul className="pagination justify-content-center">
              <li><a href="#"><i className="icon icon-arrow-left" /></a></li>
              <li className="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#"><i className="icon icon-arrow-right" /></a></li>
            </ul>
            {/* Pagination End */}
          </div>
          {/* paging End */}
        </div>
        {/* Content Col end */}
        
        
<div className="col-lg-4 col-md-12">
          <div className="sidebar sidebar-right">
            <div className="widget widget-search">
              <div id="search" className="input-group">
                <input className="form-control" placeholder="Rechercher" type="search" />
                <span>
                  <i className="icon icon-search" />
                </span>
              </div>
              {/* End Input Group */}
            </div>
            {/* End Widget Search */}
            <div className="widget widget-cat">
              <h3 className="widget-title">Catégories</h3>
              <span className="animate-border border-offwhite tw-mt-20" />
              <ul className="widget-list">
              {
                all.map((all , index)=><>
              
                <li>
                <a href={"category"+all.category}>Seo Marketing</a>
                    <span className="posts-count">(13)</span>
                </li>
                </>
                )}
                <li><Link to="#">Search Engine</Link>
                  <span className="posts-count">(05)</span>
                </li>
                <li><a href="#">Business</a>
                  <span className="posts-count">(06)</span>
                </li>
                <li><a href="#">Marketing</a>
                  <span className="posts-count">(11)</span>
                </li>
                <li><a href="#">We development</a>
                  <span className="posts-count">(09)</span>
                </li>
                
                <li><a href="#">Web Traffice</a>
                  <span className="posts-count">(13)</span>
                </li>
                
              </ul>
              {/* End Widget List */}
            </div>
            {/* Categories end */}
            <div className="widget widget-adds">
              <a href="#">
                <img src="assets/images/news/ad.jpg" alt className="img-fluid" />
              </a>
            </div>
            {/* End Widget Adds */}
            <div className="widget recent-posts">
              <h3 className="widget-title">Articles populaires</h3>
              <span className="animate-border border-offwhite tw-mt-20" />
              <ul className="unstyled clearfix">
                <li className="media">
                  <div className="media-left media-middle">
                    <img alt="img" src="assets/images/news/post1.jpg" />
                  </div>
                  <div className="media-body media-middle">
                    <h4 className="entry-title">
                      <Link to="newsSingle">Online marketing optimize your business</Link>
                    </h4>
                  </div>
                  <div className="clearfix" />
                </li>
                {/* 1st post end*/}
                <li className="media">
                  <div className="media-left media-middle">
                    <img alt="img" src="assets/images/news/post3.jpg" />
                  </div>
                  <div className="media-body media-middle">
                    <h4 className="entry-title">
                      <Link to="newsSingle">Online marketing optimize your business</Link>
                    </h4>
                  </div>
                  <div className="clearfix" />
                </li>
                {/* 2nd post end*/}
                <li className="media">
                  <div className="media-left media-middle">
                    <img alt="img" src="assets/images/news/post2.jpg" />
                  </div>
                  <div className="media-body media-middle">
                    <h4 className="entry-title">
                      <Link to="newsSingle">Online marketing optimize your business</Link>
                    </h4>
                  </div>
                  <div className="clearfix" />
                </li>
                {/* 3rd post end*/}
                <li className="media">
                  <div className="media-left media-middle">
                    <img alt="img" src="assets/images/news/post3.jpg" />
                  </div>
                  <div className="media-body media-middle">
                    <h4 className="entry-title">
                      <Link to="newsSingle">Online marketing optimize your business</Link>
                    </h4>
                  </div>
                  <div className="clearfix" />
                </li>
                {/* 4th post end*/}
              </ul>
            </div>
            {/* Recent post end */}
            <div className="widget widget-tags">
              <h3 className="widget-title">Tags populaires</h3>
              <span className="animate-border border-offwhite tw-mt-20" />
              <ul className="unstyled clearfix">
                <li><a href="#">Financial</a></li>
                <li><a href="#">Our Advisor</a></li>
                <li><a href="#">Euro 2017</a></li>
                <li><a href="#">Euro Business</a></li>
                <li><a href="#">Business Awards</a></li>
                <li><a href="#">Market</a></li>
                <li><a href="#">30K</a></li>
                <li><a href="#">Euro 2017</a></li>
              </ul>
            </div>
            {/* Tags end */}
          </div>
          {/* End Sidebar Right */}
        </div>
       
        {/* Sidebar Col end */}
      </div>
      {/* Main row end */}
    </div>
    {/* Container end */}
  </section>
  {/* Main container end */}
</>



        )
    }
}
export default AllBlog