import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import Common from "./Common";
import Preloader from "./Preloader";
import CatBack from "./CatBack";
import Filternav from "./Filternav";
class SubCategory extends Component{
        render(){
        return(
          <>
          {/* Preloader*/}
          <Preloader/>
          {/* Header Area*/}
          <CatBack
          title="Clothing"
          />
          {/* Sidenav Black Overlay*/}
            <Filternav/>
          <div className="page-content-wrapper">
            {/* Catagory Single Image*/}
            <div className="pt-3">
              <div className="container">
                <div
                  className="catagory-single-img pt-3"
                  style={{ backgroundImage: 'url("assets/img/bg-img/5.jpg")' }}
                />
              </div>
            </div>
            {/* Product Catagories*/}
            {/* Top Products*/}
            <div className="top-products-area pb-3">
              <div className="container">
                <div className="section-heading d-flex align-items-center justify-content-between">
                  <h6>Sub Catagory Products</h6>
                </div>
                <div className="row g-3">
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-success">Sale</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/11.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Beach Cap
                        </a>
                        <p className="sale-price">
                          $13<span>$42</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-primary">New</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/5.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Wooden Sofa
                        </a>
                        <p className="sale-price">
                          $74<span>$99</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-success">Sale</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/6.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Roof Lamp
                        </a>
                        <p className="sale-price">
                          $99<span>$113</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-danger">-15%</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/9.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Sneaker Shoes
                        </a>
                        <p className="sale-price">
                          $87<span>$92</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-danger">-11%</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/8.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Wooden Chair
                        </a>
                        <p className="sale-price">
                          $21<span>$25</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-warning">Hot</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/4.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Polo Shirts
                        </a>
                        <p className="sale-price">
                          $38<span>$41</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-success">Sale</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/11.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Beach Cap
                        </a>
                        <p className="sale-price">
                          $13<span>$42</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-primary">New</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/5.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Wooden Sofa
                        </a>
                        <p className="sale-price">
                          $74<span>$99</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-success">Sale</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/6.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Roof Lamp
                        </a>
                        <p className="sale-price">
                          $99<span>$113</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-danger">-15%</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/9.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Sneaker Shoes
                        </a>
                        <p className="sale-price">
                          $87<span>$92</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-danger">-11%</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/8.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Wooden Chair
                        </a>
                        <p className="sale-price">
                          $21<span>$25</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* Single Top Product Card*/}
                  <div className="col-6 col-md-4 col-lg-3">
                    <div className="card top-product-card">
                      <div className="card-body">
                        <span className="badge badge-warning">Hot</span>
                        <a className="wishlist-btn" href="#">
                          <i className="lni lni-heart" />
                        </a>
                        <a
                          className="product-thumbnail d-block"
                          href="/single"
                        >
                          <img className="mb-2" src="assets/img/product/4.png" alt />
                        </a>
                        <a className="product-title d-block" href="/single">
                          Polo Shirts
                        </a>
                        <p className="sale-price">
                          $38<span>$41</span>
                        </p>
                        <div className="product-rating">
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                          <i className="lni lni-star-filled" />
                        </div>
                        <a className="btn btn-success btn-sm" href="#">
                          <i className="lni lni-plus" />
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Internet Connection Status*/}
          <div className="internet-connection-status" id="internetStatus" />
          <Common/>
        </>
        



        )
    }
}
export default SubCategory