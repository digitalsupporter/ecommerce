
import React, {Component} from 'react';

import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

const Sidenav = (props) => {
        return(
            <>

<div className="sidenav-black-overlay" />
  {/* Side Nav Wrapper*/}
  <div className="suha-sidenav-wrapper"  id="sidenavWrapper">
    {/* Sidenav Profile*/}
    <div className="sidenav-profile">
      <div className="user-profile">
        <img src="assets/img/bg-img/9.jpg" alt />
      </div>
      <div className="user-info">
        <h6 className="user-name mb-0">Suha Jannat</h6>
        <p className="available-balance">
          Balance{" "}
          <span>
            $<span className="counter">523.98</span>
          </span>
        </p>
      </div>
    </div>
    {/* Sidenav Nav*/}
    <ul className="sidenav-nav ps-0">
      <li>
        <Link to="profile.html">
          <i className="lni lni-user" />
          My Profile
        </Link>
      </li>
      <li>
        <Link to="notifications.html">
          <i className="lni lni-alarm lni-tada-effect" />
          Notifications<span className="ms-3 badge badge-warning">3</span>
        </Link>
      </li>
      <li className="suha-dropdown-menu">
        <Link to="#">
          <i className="lni lni-cart" />
          Shop Pages
        </Link>
        <ul>
          <li>
            <Link to="shop-grid.html">- Shop Grid</Link>
          </li>
          <li>
            <Link to="shop-list.html">- Shop List</Link>
          </li>
          <li>
            <Link to="single-product.html">- Product Details</Link>
          </li>
          <li>
            <Link to="featured-products.html">- Featured Products</Link>
          </li>
          <li>
            <Link to="flash-sale.html">- Flash Sale</Link>
          </li>
        </ul>
      </li>
      <li>
        <Link to="pages.html">
          <i className="lni lni-empty-file" />
          All Pages
        </Link>
      </li>
      <li className="suha-dropdown-menu">
        <Link to="wishlist-grid.html">
          <i className="lni lni-heart" />
          My Wishlist
        </Link>
        <ul>
          <li>
            <Link to="wishlist-grid.html">- Wishlist Grid</Link>
          </li>
          <li>
            <Link to="wishlist-list.html">- Wishlist List</Link>
          </li>
        </ul>
      </li>
      <li>
        <Link to="settings.html">
          <i className="lni lni-cog" />
          Settings
        </Link>
      </li>
      <li>
        <Link to="intro.html">
          <i className="lni lni-power-switch" />
          Sign Out
        </Link>
      </li>
    </ul>
    {/* Go Back Button*/}
    <div className="go-home-btn" id="goHomeBtn">
      <i className="lni lni-arrow-left" />
    </div>
  </div>
  
</>	
)
}

export default Sidenav