import React, {Component} from 'react';

import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

const Filternav = (props) => {
        return(
            <>

<div className="sidenav-black-overlay" />
          {/* Side Nav Wrapper*/}
          <div className="suha-sidenav-wrapper filter-nav" id="sidenavWrapper">
            {/* Catagory Sidebar Area*/}
            <div className="catagory-sidebar-area">
              {/* Catagory*/}
              <div className="widget catagory mb-4">
                <h6 className="widget-title mb-3">Product Brand</h6>
                <div className="widget-desc">
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="zara"
                      type="checkbox"
                      defaultChecked
                    />
                    <label className="form-check-label font-weight-bold" htmlFor="zara">
                      Zara
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="Gucci" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="Gucci"
                    >
                      Gucci
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="Addidas" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="Addidas"
                    >
                      Addidas
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="Nike" type="checkbox" />
                    <label className="form-check-label font-weight-bold" htmlFor="Nike">
                      Nike
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="Denim" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="Denim"
                    >
                      Denim
                    </label>
                  </div>
                </div>
              </div>
              {/* Color*/}
              <div className="widget color mb-4">
                <h6 className="widget-title mb-3">Color Family</h6>
                <div className="widget-desc">
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="Purple"
                      type="checkbox"
                      defaultChecked
                    />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="Purple"
                    >
                      Purple
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="Black" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="Black"
                    >
                      Black
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="White" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="White"
                    >
                      White
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="Red" type="checkbox" />
                    <label className="form-check-label font-weight-bold" htmlFor="Red">
                      Red
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="Pink" type="checkbox" />
                    <label className="form-check-label font-weight-bold" htmlFor="Pink">
                      Pink
                    </label>
                  </div>
                </div>
              </div>
              {/* Size*/}
              <div className="widget size mb-4">
                <h6 className="widget-title mb-3">Size</h6>
                <div className="widget-desc">
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="XtraLarge"
                      type="checkbox"
                      defaultChecked
                    />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="XtraLarge"
                    >
                      Xtra Large
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="Large" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="Large"
                    >
                      Large
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="medium" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="medium"
                    >
                      Medium
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="Small" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="Small"
                    >
                      Small
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="ExtraSmall"
                      type="checkbox"
                    />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="ExtraSmall"
                    >
                      Extra Small
                    </label>
                  </div>
                </div>
              </div>
              {/* Ratings*/}
              <div className="widget ratings mb-4">
                <h6 className="widget-title mb-3">Ratings</h6>
                <div className="widget-desc">
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="5star"
                      type="checkbox"
                      defaultChecked
                    />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="5star"
                    >
                      5 star
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="4star" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="4star"
                    >
                      4 star
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="3star" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="3star"
                    >
                      3 star
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="2star" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="2star"
                    >
                      2 star
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="1star" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="1star"
                    >
                      1 star
                    </label>
                  </div>
                </div>
              </div>
              {/* Payment Type*/}
              <div className="widget payment-type mb-4">
                <h6 className="widget-title mb-3">Payment Type</h6>
                <div className="widget-desc">
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="cod"
                      type="checkbox"
                      defaultChecked
                    />
                    <label className="form-check-label font-weight-bold" htmlFor="cod">
                      Cash On Delivery
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="paypal" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="paypal"
                    >
                      Paypal
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="checkpayment"
                      type="checkbox"
                    />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="checkpayment"
                    >
                      Check Payment
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input className="form-check-input" id="payonner" type="checkbox" />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="payonner"
                    >
                      Payonner
                    </label>
                  </div>
                  {/* Single Checkbox*/}
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="mobbanking"
                      type="checkbox"
                    />
                    <label
                      className="form-check-label font-weight-bold"
                      htmlFor="mobbanking"
                    >
                      Mobile Banking
                    </label>
                  </div>
                </div>
              </div>
              {/* Apply Filter*/}
              <div className="apply-filter-btn">
                <a className="btn btn-success" href="#">
                  Apply Filter
                </a>
              </div>
            </div>
            {/* Go Back Button*/}
            <div className="go-home-btn" id="goHomeBtn">
              <i className="lni lni-arrow-left" />
            </div>
          </div>
          
</>

	
)
}

export default Filternav