import React,{Component} from 'react';
import {Link, Redirect} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Common from "./Common";
import Back from "./Back";
import Preloader from "./Preloader";
import Sidenav from "./Sidenav";
import { withCookies } from "react-cookie";
class Single extends Component{
  state = {
    all:[]
  };
  constructor(props) {
    super(props);
    this.handleClickAdd = this.handleClickAdd;
    this.user = this.user;
  }
  user =()=>{
    const user = this.props.cookies.get("user");
    if(user){
       console.log(user);
       
    }else{
      return <Redirect to='/login'/>
    }
   }
  handleClickAdd=(e)=> {
    // console.log(e.target.id);
    console.log(e.currentTarget.id);
    let id = e.currentTarget.id;
    let item;
    const user = this.props.cookies.get("user");
    item='1';
    // alert("Fetch Api code here");
    var InsertAPIURL="https://digitalsupporter.in/reactjs/allApi/seo/addtocard.php";
    var headers={
        'Accept':'application/json',
        'Contect-Type':'application/json'
   };
   var Data={
       product_id:id,
       user:user,
       item:item,
       
   };
   fetch(InsertAPIURL,
       {
           method:'POST',
           headers: headers,
           body:JSON.stringify(Data)
       }
   )
   .then((response)=>response.json())
   .then((response)=>
   {
     console.log(response);
    let cartval = document.getElementById("cartval");
    cartval.innerHTML = response.cart;
    
   })
   .catch((error)=>
   {
       alert("Error"+error);
       console.log(error);
       this.setState({authError: true, isLoading: true});
       
   })
  }
async componentDidMount() {
  var headers={
    'Accept':'application/json',
    'Contect-Type':'application/json'
}; 
  const nid = this.props.match.params.pid;
  console.log('mukesh')
  console.log(nid)
  let ta={
    nid:nid    
};
   const url = "http://digitalsupporter.in/reactjs/allApi/seo/single.php";
   const response = await fetch(url,
    {
        method:'POST',
        headers: headers,
        body:JSON.stringify(ta)
        
    });
   const data = await response.json();
   
   console.log(data);
   console.log(55);
    this.setState({all:data[0][0]})
  //  console.log(data[nid]);
  //   this.setState({all:data[nid]})
    // this.setState({all:data})
  //  console.log(address);
  // data.forEach(element => {
    
  // });
}
 
    render(){
      const all=this.state.all
      console.log(55);
      console.log(all);
      console.log(this.props);

      var img = all.image;
      console.log(img)
      console.log('img')
        return(
      <>
            
  {/* Preloader*/}
  <Preloader/>
  {/* Header Area*/}
 
        <Back
        title="Product Details"
        />
        
  {/* Sidenav Black Overlay*/}
          <Sidenav/>
  {/* PWA Install Alert*/}
  <div className="page-content-wrapper">
    {/* Product Slides*/}
    <OwlCarousel className="hero-slides owl-carousel"
        items={1}
        loop={true}
        nav={true}
        autoplay={true}
        dots={true}
        navText={['<i class="lni lni-chevron-left"></i>','<i class="lni lni-chevron-right"></i>']}
        className="product-slides owl-carousel">
      {/* Single Hero Slide*/}
    
      <div
        className="single-product-slide"
        style={{ backgroundImage: 'url({img})'
       }}
      />
      {/* Single Hero Slide*/}
      <div
        className="single-product-slide"
        style={{ backgroundImage: 'url("assets/img/bg-img/10.jpg")' }}
      />
      {/* Single Hero Slide*/}
      <div
        className="single-product-slide"
        style={{ backgroundImage: 'url("assets/img/bg-img/11.jpg")' }}
      />
    </OwlCarousel>
    <div className="product-description pb-3">
      {/* Product Title & Meta Data*/}
      <div className="product-title-meta-data bg-white mb-3 py-3">
        <div className="container d-flex justify-content-between">
          <div className="p-title-price">
            <h6 className="mb-1">{all.product_name}</h6>
            <p className="sale-price mb-0">
              ${all.price_after}<span>${all.price_before}</span>
            </p>
          </div>
          <div className="p-wishlist-share">
            <a href="wishlist-grid.html">
              <i className="lni lni-heart" />
            </a>
          </div>
        </div>
        {/* Ratings*/}
        <div className="product-ratings">
          <div className="container d-flex align-items-center justify-content-between">
            <div className="ratings">
              <i className="lni lni-star-filled" />
              <i className="lni lni-star-filled" />
              <i className="lni lni-star-filled" />
              <i className="lni lni-star-filled" />
              <i className="lni lni-star-filled" />
              <span className="ps-1">3 ratings</span>
            </div>
            <div className="total-result-of-ratings">
              <span>5.0</span>
              <span>Very Good </span>
            </div>
          </div>
        </div>
      </div>
      {/* Flash Sale Panel*/}
      <div className="flash-sale-panel bg-white mb-3 py-3">
        <div className="container">
          {/* Sales Offer Content*/}
          <div className="sales-offer-content d-flex align-items-end justify-content-between">
            {/* Sales End*/}
            <div className="sales-end">
              <p className="mb-1 font-weight-bold">
                <i className="lni lni-bolt" /> Flash sale end in
              </p>
              {/* Please use event time this format: YYYY/MM/DD hh:mm:ss*/}
              <ul
                className="sales-end-timer ps-0 d-flex align-items-center"
                data-countdown="2022/01/01 14:21:37"
              >
                <li>
                  <span className="days">0</span>d
                </li>
                <li>
                  <span className="hours">0</span>h
                </li>
                <li>
                  <span className="minutes">0</span>m
                </li>
                <li>
                  <span className="seconds">0</span>s
                </li>
              </ul>
            </div>
            {/* Sales Volume*/}
            <div className="sales-volume text-end">
              <p className="mb-1 font-weight-bold">82% Sold Out</p>
              <div className="progress" style={{ height: 6 }}>
                <div
                  className="progress-bar bg-warning"
                  role="progressbar"
                  style={{ width: "82%" }}
                  aria-valuenow={82}
                  aria-valuemin={0}
                  aria-valuemax={100}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Selection Panel*/}
      <div className="selection-panel bg-white mb-3 py-3">
        <div className="container d-flex align-items-center justify-content-between">
          {/* Choose Color*/}
          <div className="choose-color-wrapper">
            <p className="mb-1 font-weight-bold">Color</p>
            <div className="choose-color-radio d-flex align-items-center">
              {/* Single Radio Input*/}
              <div className="form-check mb-0">
                <input
                  className="form-check-input blue"
                  id="colorRadio1"
                  type="radio"
                  name="colorRadio"
                  defaultChecked
                />
                <label className="form-check-label" htmlFor="colorRadio1" />
              </div>
              {/* Single Radio Input*/}
              <div className="form-check mb-0">
                <input
                  className="form-check-input yellow"
                  id="colorRadio2"
                  type="radio"
                  name="colorRadio"
                />
                <label className="form-check-label" htmlFor="colorRadio2" />
              </div>
              {/* Single Radio Input*/}
              <div className="form-check mb-0">
                <input
                  className="form-check-input green"
                  id="colorRadio3"
                  type="radio"
                  name="colorRadio"
                />
                <label className="form-check-label" htmlFor="colorRadio3" />
              </div>
              {/* Single Radio Input*/}
              <div className="form-check mb-0">
                <input
                  className="form-check-input purple"
                  id="colorRadio4"
                  type="radio"
                  name="colorRadio"
                />
                <label className="form-check-label" htmlFor="colorRadio4" />
              </div>
            </div>
          </div>
          {/* Choose Size*/}
          <div className="choose-size-wrapper text-end">
            <p className="mb-1 font-weight-bold">Size</p>
            <div className="choose-size-radio d-flex align-items-center">
              {/* Single Radio Input*/}
              <div className="form-check mb-0 me-2">
                <input
                  className="form-check-input"
                  id="sizeRadio1"
                  type="radio"
                  name="sizeRadio"
                />
                <label className="form-check-label" htmlFor="sizeRadio1">
                  S
                </label>
              </div>
              {/* Single Radio Input*/}
              <div className="form-check mb-0 me-2">
                <input
                  className="form-check-input"
                  id="sizeRadio2"
                  type="radio"
                  name="sizeRadio"
                  defaultChecked
                />
                <label className="form-check-label" htmlFor="sizeRadio2">
                  M
                </label>
              </div>
              {/* Single Radio Input*/}
              <div className="form-check mb-0">
                <input
                  className="form-check-input"
                  id="sizeRadio3"
                  type="radio"
                  name="sizeRadio"
                />
                <label className="form-check-label" htmlFor="sizeRadio3">
                  L
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Add To Cart*/}
      <div className="cart-form-wrapper bg-white mb-3 py-3">
        <div className="container">
          <div className="cart-form" >
            <div className="order-plus-minus d-flex align-items-center">
              <div className="quantity-button-handler">-</div>
              <input
                className="form-control cart-quantity-input"
                type="text"
                step={1}
                name="quantity"
                defaultValue={all.item}
              />
              <div className="quantity-button-handler">+</div>
            </div>
            <button className="btn btn-danger ms-3"  id={all.product_id} onClick={this.handleClickAdd} >
              Add To Cart
            </button>
          </div>
        </div>
      </div>
      {/* Product Specification*/}
      <div className="p-specification bg-white mb-3 py-3">
        <div className="container">
          <h6>Specifications</h6>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi, eum?
            Id, culpa? At officia quisquam laudantium nisi mollitia nesciunt,
            qui porro asperiores cum voluptates placeat similique recusandae in
            facere quos vitae?
          </p>
          <ul className="mb-3 ps-3">
            <li>
              <i className="lni lni-checkmark-circle"> </i> 100% Good Reviews
            </li>
            <li>
              <i className="lni lni-checkmark-circle"> </i> 7 Days Returns
            </li>
            <li>
              {" "}
              <i className="lni lni-checkmark-circle"> </i> Warranty not
              Aplicable
            </li>
            <li>
              {" "}
              <i className="lni lni-checkmark-circle"> </i> 100% Brand New
              Product
            </li>
          </ul>
          <p className="mb-0">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi, eum?
            Id, culpa? At officia quisquam laudantium nisi mollitia nesciunt,
            qui porro asperiores cum voluptates placeat similique recusandae in
            facere quos vitae?
          </p>
        </div>
      </div>
      {/* Rating & Review Wrapper*/}
      <div className="rating-and-review-wrapper bg-white py-3 mb-3">
        <div className="container">
          <h6>Ratings &amp; Reviews</h6>
          <div className="rating-review-content">
            <ul className="ps-0">
              <li className="single-user-review d-flex">
                <div className="user-thumbnail">
                  <img src="assets/img/bg-img/7.jpg" alt />
                </div>
                <div className="rating-comment">
                  <div className="rating">
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                  </div>
                  <p className="comment mb-0">
                    Very good product. It's just amazing!
                  </p>
                  <span className="name-date">Designing World 12 Dec 2021</span>
                </div>
              </li>
              <li className="single-user-review d-flex">
                <div className="user-thumbnail">
                  <img src="assets/img/bg-img/8.jpg" alt />
                </div>
                <div className="rating-comment">
                  <div className="rating">
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                  </div>
                  <p className="comment mb-0">
                    Very excellent product. Love it.
                  </p>
                  <span className="name-date">Designing World 8 Dec 2021</span>
                </div>
              </li>
              <li className="single-user-review d-flex">
                <div className="user-thumbnail">
                  <img src="assets/img/bg-img/9.jpg" alt />
                </div>
                <div className="rating-comment">
                  <div className="rating">
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                    <i className="lni lni-star-filled" />
                  </div>
                  <p className="comment mb-0">
                    What a nice product it is. I am looking it's.
                  </p>
                  <span className="name-date">Designing World 28 Nov 2021</span>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      {/* Ratings Submit Form*/}
      <div className="ratings-submit-form bg-white py-3">
        <div className="container">
          <h6>Submit A Review</h6>
          <form action="#" method>
            <div className="stars mb-3">
              <input className="star-1" type="radio" name="star" id="star1" />
              <label className="star-1" htmlFor="star1" />
              <input className="star-2" type="radio" name="star" id="star2" />
              <label className="star-2" htmlFor="star2" />
              <input className="star-3" type="radio" name="star" id="star3" />
              <label className="star-3" htmlFor="star3" />
              <input className="star-4" type="radio" name="star" id="star4" />
              <label className="star-4" htmlFor="star4" />
              <input className="star-5" type="radio" name="star" id="star5" />
              <label className="star-5" htmlFor="star5" />
              <span />
            </div>
            <textarea
              className="form-control mb-3"
              id="comments"
              name="comment"
              cols={30}
              rows={10}
              data-max-length={200}
              placeholder="Write your review..."
              defaultValue={""}
            />
            <button className="btn btn-sm btn-primary" type="submit">
              Save Review
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
  {/* Internet Connection Status*/}
  <div className="internet-connection-status" id="internetStatus" />
  <Common/>
</>

            )
    }
}
export default withCookies(Single)