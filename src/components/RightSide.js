import React, {Component} from 'react';

import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

const RightSide = (props) => {
        return(
            <>


<div className="col-lg-4 col-md-12">
          <div className="sidebar sidebar-right">
            <div className="widget widget-search">
              <div id="search" className="input-group">
                <input className="form-control" placeholder="Rechercher" type="search" />
                <span>
                  <i className="icon icon-search" />
                </span>
              </div>
              {/* End Input Group */}
            </div>
            {/* End Widget Search */}
            <div className="widget widget-cat">
              <h3 className="widget-title">Catégories</h3>
              <span className="animate-border border-offwhite tw-mt-20" />
              <ul className="widget-list">
                <li><Link to="#">Search Engine</Link>
                  <span className="posts-count">(05)</span>
                </li>
                <li><a href="#">Business</a>
                  <span className="posts-count">(06)</span>
                </li>
                <li><a href="#">Marketing</a>
                  <span className="posts-count">(11)</span>
                </li>
                <li><a href="#">We development</a>
                  <span className="posts-count">(09)</span>
                </li>
                <li><a href="#">Seo Marketing</a>
                  <span className="posts-count">(13)</span>
                </li>
                <li><a href="#">Web Traffice</a>
                  <span className="posts-count">(13)</span>
                </li>
              </ul>
              {/* End Widget List */}
            </div>
            {/* Categories end */}
            <div className="widget widget-adds">
              <a href="#">
                <img src="assets/images/news/ad.jpg" alt className="img-fluid" />
              </a>
            </div>
            {/* End Widget Adds */}
            <div className="widget recent-posts">
              <h3 className="widget-title">Articles populaires</h3>
              <span className="animate-border border-offwhite tw-mt-20" />
              <ul className="unstyled clearfix">
                <li className="media">
                  <div className="media-left media-middle">
                    <img alt="img" src="assets/images/news/post1.jpg" />
                  </div>
                  <div className="media-body media-middle">
                    <h4 className="entry-title">
                      <Link to="newsSingle">Online marketing optimize your business</Link>
                    </h4>
                  </div>
                  <div className="clearfix" />
                </li>
                {/* 1st post end*/}
                <li className="media">
                  <div className="media-left media-middle">
                    <img alt="img" src="assets/images/news/post3.jpg" />
                  </div>
                  <div className="media-body media-middle">
                    <h4 className="entry-title">
                      <Link to="newsSingle">Online marketing optimize your business</Link>
                    </h4>
                  </div>
                  <div className="clearfix" />
                </li>
                {/* 2nd post end*/}
                <li className="media">
                  <div className="media-left media-middle">
                    <img alt="img" src="assets/images/news/post2.jpg" />
                  </div>
                  <div className="media-body media-middle">
                    <h4 className="entry-title">
                      <Link to="newsSingle">Online marketing optimize your business</Link>
                    </h4>
                  </div>
                  <div className="clearfix" />
                </li>
                {/* 3rd post end*/}
                <li className="media">
                  <div className="media-left media-middle">
                    <img alt="img" src="assets/images/news/post3.jpg" />
                  </div>
                  <div className="media-body media-middle">
                    <h4 className="entry-title">
                      <Link to="newsSingle">Online marketing optimize your business</Link>
                    </h4>
                  </div>
                  <div className="clearfix" />
                </li>
                {/* 4th post end*/}
              </ul>
            </div>
            {/* Recent post end */}
            <div className="widget widget-tags">
              <h3 className="widget-title">Tags populaires</h3>
              <span className="animate-border border-offwhite tw-mt-20" />
              <ul className="unstyled clearfix">
                <li><a href="#">Financial</a></li>
                <li><a href="#">Our Advisor</a></li>
                <li><a href="#">Euro 2017</a></li>
                <li><a href="#">Euro Business</a></li>
                <li><a href="#">Business Awards</a></li>
                <li><a href="#">Market</a></li>
                <li><a href="#">30K</a></li>
                <li><a href="#">Euro 2017</a></li>
              </ul>
            </div>
            {/* Tags end */}
          </div>
          {/* End Sidebar Right */}
        </div>
        </>

	
        )
    }

export default RightSide