import React, {Component, useEffect} from 'react';

import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { useCookies } from "react-cookie";
const Common = (props) => {
  const [cookies, setCookie] = useCookies();
 let user=cookies.user
  var InsertAPIURL="https://digitalsupporter.in/reactjs/allApi/seo/";
  var headers={'Accept':'application/json','Contect-Type':'application/json'};
  var Data={user:user};
  const getCartItem = async () => {
    const response = await fetch(InsertAPIURL+'cart_item.php',
      {method:'POST',headers: headers,body:JSON.stringify(Data)}
      );
      const data = await response.json();
      let cartval = document.getElementById("cartval");
      cartval.innerHTML = data.cart;
  }
    getCartItem();
        return(
            <>
            
            <div className="footer-nav-area" id="footerNav">
    <div className="container h-100 px-0">
      <div className="suha-footer-nav h-100">
        <ul className="h-100 d-flex align-items-center justify-content-between ps-0">
          <li className="active">
            <a href="/">
              <i className="lni lni-home" />
              Home 
            </a>
          </li>
          <li>
            <a href="/Message">
              <i className="lni lni-life-ring" />
              Support
            </a>
          </li>
          <li>
            <a href="/Cart">
              
              <i className="lni lni-shopping-basket" ><sup style={{color:"red"}} id="cartval">1</sup></i>
              Cart
            </a>
          </li>
          <li>
            <a href="/Wishlist">
              <i className="lni lni-heart" />
              Wishlist
            </a>
          </li>
          <li>
            <a href="/Settings">
              <i className="lni lni-cog" />
              Settings
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>

            </>

	
        )
    }

export default Common