import React, {Component} from 'react';

import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

const Comment = (props) => {
        return(
            <>
<section id="tw-testimonial" className="tw-testimonial">
<div className="container">
   <div className="row align-items-center justify-content-center">
     <div className="col-md-6 wow fadeInLeft" data-wow-duration="1s">
       <div className="tw-testimonial-content text-center">
         <h2 className="section-title">L'amour des clients</h2>
         <span className="animate-border border-green tw-mt-20 tw-mb-40 ml-auto mr-auto"></span>
       </div>
       <OwlCarousel className="tw-testimonial-carousel owl-carousel"
       items={1}
   loop={true}
   nav={false}
   autoplay={true}
   dots={true}
   
       >
         <div className="tw-testimonial-wrapper">
            <div className="testimonial-bg testimonial-bg-orange">
              <div className="testimonial-icon">
                <img src="assets/images/icon-image/testimonial1.png" alt="" className="img-fluid"/>
              </div>
            </div>
            {/* End Testimonial bg */}
            <div className="testimonial-text">
              <p>Commencez à travailler avec une entreprise qui peut faire tout ce dont vous avez besoin pour générer de la notoriété,
                 générer du trafic, se connecter avec</p>
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
                <i className="icon icon-quote2"></i>
              </div>
              {/* End Testimonial Meta */}
            </div>
            {/* End testimonial text */}
         </div>
         {/* End Tw testimonial wrapper */}
         <div className="tw-testimonial-wrapper">
            <div className="testimonial-bg testimonial-bg-orange">
              <div className="testimonial-icon">
                <img src="assets/images/icon-image/testimonial1.png" alt="" className="img-fluid"/>
              </div>
            </div>
            {/* End Testimonial bg */}
            <div className="testimonial-text">
              <p>Commencez à travailler avec une entreprise qui peut faire tout ce dont vous avez besoin pour générer de la notoriété,
                 générer du trafic, se connecter avec</p>
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
                <i className="icon icon-quote2"></i>
              </div>
              {/* End Testimonial meta */}
            </div>
            {/* End Testimonial Text */}
         </div>
         {/* End Tw testimonial wrapper */}
         <div className="tw-testimonial-wrapper">
            <div className="testimonial-bg testimonial-bg-orange">
              <div className="testimonial-icon">
                <img src="assets/images/icon-image/testimonial1.png" alt="" className="img-fluid"/>
              </div>
            </div>
            {/* end testimonial bg */}
            <div className="testimonial-text">
              <p>Commencez à travailler avec une entreprise qui peut faire tout ce dont vous avez besoin pour générer de la notoriété,
                 générer du trafic, se connecter avec</p>
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
                <i className="icon icon-quote2"></i>
              </div>
              {/* End testimonial meta */}
            </div>
            {/* End testimonial text */}
         </div>
         {/* End tw testimonial wrapper */}
       </OwlCarousel>
       {/* End Tw testimonial carousel */}
     </div>
     {/* End Col */}
   </div>
   {/* End Row */}
</div>
{/* End Container */}
</section>
</>

	
        )
    }

export default Comment
