import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Common from "./Common";
class Testimonials extends Component{
    render(){
        return(
			<>
            <Common
    title="Testimonials"
  />
  <section id="main-container" className="main-container">
    <div className="container">
      <div className="row">
        <div className="col text-center">
          <div className="section-heading">
            <h2>
              <small>Client’s Love</small>
              Love from <span>Client</span>
            </h2>
            <span className="animate-border ml-auto mr-auto tw-mt-20 tw-mb-40" />
          </div>
        </div>
        {/* COl End */}
      </div>
      {/* Row End */}
      <div className="row justify-content-center">
        <div className="col-md-8 text-center">
          <OwlCarousel className="testimonial-slider owl-carousel"
          items={1}
   loop={true}
   nav={false}
   autoplay={true}
   dots={false}
          >
            <div className="testimonial-content">
              <div className="testimonial-image">
                <img src="assets/images/icon-image/author.jpg" alt />
              </div>
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
                <i className="icon icon-quote2" />
              </div>
              {/* Testimonial Meta end */}
              <div className="testimonial-text">
                <p>Start working with an company that can do provide every thing at you need to generate awareness,
                  drive traffic, connect with</p>
              </div>
              {/* TEstimonial text end */}
            </div>
            {/* Testimonial Content End */}
            <div className="testimonial-content">
              <div className="testimonial-image">
                <img src="assets/images/icon-image/avatar1.jpg" alt />
              </div>
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
                <i className="icon icon-quote2" />
              </div>
              {/* Testimonial Meta end */}
              <div className="testimonial-text">
                <p>Start working with an company that can do provide every thing at you need to generate awareness,
                  drive traffic, connect with</p>
              </div>
              {/* TEstimonial text end */}
            </div>
            {/* Testimonial Content End */}
            <div className="testimonial-content">
              <div className="testimonial-image">
                <img src="assets/images/icon-image/avatar1.jpg" alt />
              </div>
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
                <i className="icon icon-quote2" />
              </div>
              {/* Testimonial Meta end */}
              <div className="testimonial-text">
                <p>Start working with an company that can do provide every thing at you need to generate awareness,
                  drive traffic, connect with</p>
              </div>
              {/* TEstimonial text end */}
            </div>
            {/* Testimonial Content End */}
          </OwlCarousel>
          {/* Carousel End */}
        </div>
        {/* COl End */}
      </div>
      {/* Row End */}
    </div>
    {/* Container End */}
  </section>
  {/* Testimonial end */}
  <section id="tw-testimonial-bg" className="tw-testimonial-bg">
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <OwlCarousel className="testimonial-box-carousel owl-carousel"
          items={3}
   loop={true}
   nav={false}
   autoplay={true}
   dots={true}
          >
            <div className="tw-testimonial-box">
              <div className="testimonial-bg testimonial-bg-orange">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
              </div>
              {/* End Testimonial Meta */}
              <div className="testimonial-text">
                <p>One of the top 100 advertising more of marketing agencies knows what into an means to be the best</p>
                <i className="icon icon-quote2" />
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
            <div className="tw-testimonial-box">
              <div className="testimonial-bg testimonial-bg-green">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
              </div>
              {/* End Testimonial Meta */}
              <div className="testimonial-text">
                <p>One of the top 100 advertising more of marketing agencies knows what into an means to be the best</p>
                <i className="icon icon-quote2" />
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
            <div className="tw-testimonial-box">
              <div className="testimonial-bg testimonial-bg-yellow">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
              </div>
              {/* End Testimonial Meta */}
              <div className="testimonial-text">
                <p>One of the top 100 advertising more of marketing agencies knows what into an means to be the best</p>
                <i className="icon icon-quote2" />
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
            <div className="tw-testimonial-box">
              <div className="testimonial-bg testimonial-bg-yellow">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
              </div>
              {/* End Testimonial Meta */}
              <div className="testimonial-text">
                <p>One of the top 100 advertising more of marketing agencies knows what into an means to be the best</p>
                <i className="icon icon-quote2" />
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
            <div className="tw-testimonial-box">
              <div className="testimonial-bg testimonial-bg-yellow">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
              </div>
              {/* End Testimonial Meta */}
              <div className="testimonial-text">
                <p>One of the top 100 advertising more of marketing agencies knows what into an means to be the best</p>
                <i className="icon icon-quote2" />
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
            <div className="tw-testimonial-box">
              <div className="testimonial-bg testimonial-bg-yellow">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
              </div>
              {/* End Testimonial Meta */}
              <div className="testimonial-text">
                <p>One of the top 100 advertising more of marketing agencies knows what into an means to be the best</p>
                <i className="icon icon-quote2" />
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
            <div className="tw-testimonial-box">
              <div className="testimonial-bg testimonial-bg-yellow">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
              </div>
              {/* End Testimonial Meta */}
              <div className="testimonial-text">
                <p>One of the top 100 advertising more of marketing agencies knows what into an means to be the best</p>
                <i className="icon icon-quote2" />
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
            <div className="tw-testimonial-box">
              <div className="testimonial-bg testimonial-bg-yellow">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
              </div>
              {/* End Testimonial Meta */}
              <div className="testimonial-text">
                <p>One of the top 100 advertising more of marketing agencies knows what into an means to be the best</p>
                <i className="icon icon-quote2" />
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
            <div className="tw-testimonial-box">
              <div className="testimonial-bg testimonial-bg-yellow">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-meta">
                <h4>
                  Jason Stattham
                  <small>CEO Microhost</small>
                </h4>
              </div>
              {/* End Testimonial Meta */}
              <div className="testimonial-text">
                <p>One of the top 100 advertising more of marketing agencies knows what into an means to be the best</p>
                <i className="icon icon-quote2" />
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
          </OwlCarousel>
          {/* Col end */}
        </div>
        {/* Row End */}
      </div>
      {/* Carousel End */}
    </div>
    {/* Container End */}
  </section>
  {/* Testimonial end */}
  <section id="tw-testimonial-bg-gray" className="tw-testimonial-no-bg">
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <OwlCarousel className="testimonial-carousel-gray owl-carousel"
          items={2}
   loop={true}
   nav={false}
   autoplay={false}
   dots={true}
          >
            <div className="tw-testimonial-wrapper testimonial-gray">
              <div className="testimonial-bg testimonial-bg-orange">
                <div className="testimonial-icon">
                  <img src="assets/images/icon-image/testimonial1.png" alt className="img-fluid" />
                </div>
              </div>
              {/* End Testimonial bg */}
              <div className="testimonial-text">
                <p>Start working with an company that can do provide every thing at you need to generate awareness,
                  drive traffic, connect with</p>
                <div className="testimonial-meta">
                  <h4>
                    Jason Stattham
                    <small>CEO Microhost</small>
                  </h4>
                  <i className="icon icon-quote2" />
                </div>
                {/* End Testimonial Meta */}
              </div>
              {/* End testimonial text */}
            </div>
            {/* End Tw testimonial wrapper */}
          </OwlCarousel>
          {/* Col end */}
        </div>
        {/* Row End */}
      </div>
      {/* Carousel End */}
    </div>
    {/* Container End */}
  </section>
  {/* Testimonial end */}
</>


        )
    }
}
export default Testimonials