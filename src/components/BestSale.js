import React, {Component, useEffect} from 'react';
import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
const BestSale = (props) => {
    return(
            <>
<div className="weekly-best-seller-area py-3">
      <div className="container">
        <div className="section-heading d-flex align-items-center justify-content-between">
          <h6>Weekly Best Sale</h6>
          <Link className="btn btn-success btn-sm" to="shop-list.html">
            View All
          </Link>
        </div>
        <div className="row g-3">
          {/* Single Weekly Product Card*/}
          <div className="col-12 col-md-6">
            <div className="card weekly-product-card">
              <div className="card-body d-flex align-items-center">
                <div className="product-thumbnail-side">
                  <span className="badge badge-success">Sale</span>
                  <Link className="wishlist-btn" to="#">
                    <i className="lni lni-heart" />
                  </Link>
                  <Link
                    className="product-thumbnail d-block"
                    to="/single"
                  >
                    <img src="assets/img/product/10.png" alt />
                  </Link>
                </div>
                <div className="product-description">
                  <Link
                    className="product-title d-block"
                    to="/single"
                  >
                    Modern Red Sofa
                  </Link>
                  <p className="sale-price">
                    <i className="lni lni-dollar" />
                    $64<span>$89</span>
                  </p>
                  <div className="product-rating">
                    <i className="lni lni-star-filled" />
                    4.88 (39)
                  </div>
                  <Link className="btn btn-danger btn-sm" to="#">
                    <i className="me-1 lni lni-cart" />
                    Buy Now
                  </Link>
                </div>
              </div>
            </div>
          </div>
          {/* Single Weekly Product Card*/}
          <div className="col-12 col-md-6">
            <div className="card weekly-product-card">
              <div className="card-body d-flex align-items-center">
                <div className="product-thumbnail-side">
                  <span className="badge badge-primary">Sale</span>
                  <Link className="wishlist-btn" to="#">
                    <i className="lni lni-heart" />
                  </Link>
                  <Link
                    className="product-thumbnail d-block"
                    to="/single"
                  >
                    <img src="assets/img/product/7.png" alt />
                  </Link>
                </div>
                <div className="product-description">
                  <Link
                    className="product-title d-block"
                    to="/single"
                  >
                    Office Chair
                  </Link>
                  <p className="sale-price">
                    <i className="lni lni-dollar" />
                    $100<span>$160</span>
                  </p>
                  <div className="product-rating">
                    <i className="lni lni-star-filled" />
                    4.82 (125)
                  </div>
                  <Link className="btn btn-danger btn-sm" to="#">
                    <i className="me-1 lni lni-cart" />
                    Buy Now
                  </Link>
                </div>
              </div>
            </div>
          </div>
          {/* Single Weekly Product Card*/}
          <div className="col-12 col-md-6">
            <div className="card weekly-product-card">
              <div className="card-body d-flex align-items-center">
                <div className="product-thumbnail-side">
                  <span className="badge badge-danger">-10%</span>
                  <Link className="wishlist-btn" to="#">
                    <i className="lni lni-heart" />
                  </Link>
                  <Link
                    className="product-thumbnail d-block"
                    to="/single"
                  >
                    <img src="assets/img/product/12.png" alt />
                  </Link>
                </div>
                <div className="product-description">
                  <Link
                    className="product-title d-block"
                    to="/single"
                  >
                    Sun Glasses
                  </Link>
                  <p className="sale-price">
                    <i className="lni lni-dollar" />
                    $24<span>$32</span>
                  </p>
                  <div className="product-rating">
                    <i className="lni lni-star-filled" />
                    4.79 (63)
                  </div>
                  <Link className="btn btn-danger btn-sm" to="#">
                    <i className="me-1 lni lni-cart" />
                    Buy Now
                  </Link>
                </div>
              </div>
            </div>
          </div>
          {/* Single Weekly Product Card*/}
          <div className="col-12 col-md-6">
            <div className="card weekly-product-card">
              <div className="card-body d-flex align-items-center">
                <div className="product-thumbnail-side">
                  <span className="badge badge-warning">New</span>
                  <Link className="wishlist-btn" to="#">
                    <i className="lni lni-heart" />
                  </Link>
                  <Link
                    className="product-thumbnail d-block"
                    to="/single"
                  >
                    <img src="assets/img/product/13.png" alt />
                  </Link>
                </div>
                <div className="product-description">
                  <Link
                    className="product-title d-block"
                    to="/single"
                  >
                    Wall Clock
                  </Link>
                  <p className="sale-price">
                    <i className="lni lni-dollar" />
                    $31<span>$47</span>
                  </p>
                  <div className="product-rating">
                    <i className="lni lni-star-filled" />
                    4.99 (7)
                  </div>
                  <Link className="btn btn-danger btn-sm" to="#">
                    <i className="me-1 lni lni-cart" />
                    Buy Now
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    </>

	
    )
}

export default BestSale