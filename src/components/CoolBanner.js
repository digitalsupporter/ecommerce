import React, {Component, useEffect} from 'react';
import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
const CoolBanner = (props) => {
    return(
            <>
<div className="cta-area">
      <div className="container">
        <div
          className="cta-text p-4 p-lg-5"
          style={{ backgroundImage: "url(assets/img/bg-img/24.jpg)" }}
        >
          <h4>Winter Sale 50% Off</h4>
          <p>
            Suha is a multipurpose, creative &amp; <br />
            modern mobile template.
          </p>
          <Link className="btn btn-danger" to="#">
            Shop Today
          </Link>
        </div>
      </div>
    </div>
    
    </>

	
    )
}

export default CoolBanner