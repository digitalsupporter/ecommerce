import React, {Component, useEffect} from 'react';

import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
const AddToHomeScreen = (props) => {

    return(
  <div>
    <div className="toast pwa-install-alert shadow bg-white"
            role="alert"
            aria-live="assertive"
            aria-atomic="true"
            data-bs-delay={5000}
            data-bs-autohide="true"
        >
            <div className="toast-body">
            <div className="content d-flex align-items-center mb-2">
                <img src="assets/img/icons/icon-72x72.png" alt />
                <h6 className="mb-0">Add to Home Screen </h6>
                <button
                className="btn-close ms-auto"
                type="button"
                data-bs-dismiss="toast"
                aria-label="Close"
                />
            </div>
            <span className="mb-0 d-block">
                Add Suha on your mobile home screen. Click the
                <strong className="mx-1">"Add to Home Screen"</strong>button &amp; enjoy
                it like a regular app.
            </span>
            </div>
        </div>
  
  </div>

	
  )
}

export default AddToHomeScreen