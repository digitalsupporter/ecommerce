import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Common from "./Common";
import Back from "./Back";
import Preloader from "./Preloader";
import Sidenav from "./Sidenav";
class Message extends Component{
    render(){
        return(
            <>
  {/* Preloader*/}
            <Preloader/>
  {/* Internet Connection Status*/}
  <div className="internet-connection-status" id="internetStatus" />
  {/* Header Area*/}
 
  <Back
  title="Live Chat"
  />
        
        {/* Sidenav Black Overlay*/}
                <Sidenav/>
                
                <div className="suha-sidenav-wrapper" id="sidenavWrapper">
    {/* Sidenav Profile*/}
    <div className="sidenav-profile">
      <div className="user-profile">
        <img src="assets/img/bg-img/9.jpg" alt />
      </div>
      <div className="user-info">
        <h6 className="user-name mb-0">Suha Jannat</h6>
        <p className="available-balance">
          Balance{" "}
          <span>
            $<span className="counter">523.98</span>
          </span>
        </p>
      </div>
    </div>
    {/* Sidenav Nav*/}
    <ul className="sidenav-nav ps-0">
      <li>
        <a href="profile.html">
          <i className="lni lni-user" />
          My Profile
        </a>
      </li>
      <li>
        <a href="notifications.html">
          <i className="lni lni-alarm lni-tada-effect" />
          Notifications<span className="ms-3 badge badge-warning">3</span>
        </a>
      </li>
      <li className="suha-dropdown-menu">
        <a href="#">
          <i className="lni lni-cart" />
          Shop Pages
        </a>
        <ul>
          <li>
            <a href="shop-grid.html">- Shop Grid</a>
          </li>
          <li>
            <a href="shop-list.html">- Shop List</a>
          </li>
          <li>
            <a href="single-product.html">- Product Details</a>
          </li>
          <li>
            <a href="featured-products.html">- Featured Products</a>
          </li>
          <li>
            <a href="flash-sale.html">- Flash Sale</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="pages.html">
          <i className="lni lni-empty-file" />
          All Pages
        </a>
      </li>
      <li className="suha-dropdown-menu">
        <a href="wishlist-grid.html">
          <i className="lni lni-heart" />
          My Wishlist
        </a>
        <ul>
          <li>
            <a href="wishlist-grid.html">- Wishlist Grid</a>
          </li>
          <li>
            <a href="wishlist-list.html">- Wishlist List</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="settings.html">
          <i className="lni lni-cog" />
          Settings
        </a>
      </li>
      <li>
        <a href="intro.html">
          <i className="lni lni-power-switch" />
          Sign Out
        </a>
      </li>
    </ul>
    {/* Go Back Button*/}
    <div className="go-home-btn" id="goHomeBtn">
      <i className="lni lni-arrow-left" />
    </div>
  </div>
  <div className="page-content-wrapper">
    {/* Live Chat Intro*/}
    <div className="live-chat-intro mb-3">
      <p>Start a conversation</p>
      <img src="assets/img/bg-img/9.jpg" alt />
      <div className="status online">We're online</div>
      {/* Use this code for "Offline" Status*/}
      {/* .status.offline We’ll be back soon*/}
    </div>
    {/* Support Wrapper*/}
    <div className="support-wrapper py-3">
      <div className="container">
        {/* Live Chat Wrapper*/}
        <div className="live-chat-wrapper">
          {/* Agent Message Content*/}
          <div className="agent-message-content d-flex align-items-start">
            {/* Agent Thumbnail*/}
            <div className="agent-thumbnail me-2 mt-2">
              <img src="assets/img/bg-img/9.jpg" alt />
            </div>
            {/* Agent Message Text*/}
            <div className="agent-message-text">
              <div className="d-block">
                <p>
                  Hi there! You can start asking your question below. I am ready
                  to help.
                </p>
              </div>
              <div className="d-block">
                <p>How can I help you with?</p>
              </div>
              <span>12:00</span>
            </div>
          </div>
          {/* User Message Content*/}
          <div className="user-message-content">
            {/* User Message Text*/}
            <div className="user-message-text">
              <div className="d-block">
                <p>I liked your template.</p>
              </div>
              <span>12:18</span>
            </div>
          </div>
          {/* Agent Message Content*/}
          <div className="agent-message-content d-flex align-items-start">
            {/* Agent Thumbnail*/}
            <div className="agent-thumbnail me-2 mt-2">
              <img src="assets/img/bg-img/9.jpg" alt />
            </div>
            {/* Agent Message Text*/}
            <div className="agent-message-text">
              <div className="d-block">
                <p>Thank you.</p>
              </div>
              <span>12:36</span>
            </div>
          </div>
          {/* User Message Content*/}
          <div className="user-message-content">
            {/* User Message Text*/}
            <div className="user-message-text">
              <div className="d-block">
                <p>How can I buy this?</p>
              </div>
              <span>12:42</span>
            </div>
          </div>
          {/* Agent Message Content*/}
          <div className="agent-message-content d-flex align-items-start">
            {/* Agent Thumbnail*/}
            <div className="agent-thumbnail me-2 mt-2">
              <img src="assets/img/bg-img/9.jpg" alt />
            </div>
            {/* Agent Message Text*/}
            <div className="agent-message-text">
              <div className="d-block">
                <div className="writing-mode">
                  <span className="dot" />
                  <span className="dot" />
                  <span className="dot" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/* Type Message Form*/}
  <div className="type-text-form">
    <form action="#">
      <div className="form-group file-upload mb-0">
        <input type="file" />
        <i className="lni lni-plus" />
      </div>
      <textarea
        className="form-control"
        name="message"
        cols={30}
        rows={10}
        placeholder="Type your message"
        defaultValue={""}
      />
      <button type="submit">
        <svg
          className="bi bi-arrow-right"
          xmlns="http://www.w3.org/2000/svg"
          width={20}
          height={20}
          fill="currentColor"
          viewBox="0 0 16 16"
        >
          <path
            fillRule="evenodd"
            d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"
          />
        </svg>
      </button>
    </form>
  </div>

</>

            )
    }
}
export default Message