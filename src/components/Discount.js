import React, {Component, useEffect} from 'react';
import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
const Discount = (props) => {
    return(
            <>
<div className="container">
      <div className="card discount-coupon-card border-0">
        <div className="card-body">
          <div className="coupon-text-wrap d-flex align-items-center p-3">
            <h5 className="text-white pe-3 mb-0">
              Get 20% <br /> discount
            </h5>
            <p className="text-white ps-3 mb-0">
              To get discount, enter the<strong className="px-1">GET20</strong>
              code on the checkout page.
            </p>
          </div>
        </div>
      </div>
    </div>
    
    </>

	
    )
}

export default Discount