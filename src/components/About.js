import React, {Component} from 'react';

import {Link} from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Common from "./Common";
import Comment from "./Comment";


class About extends Component{
    render(){
        return(
            <>
  <Common
    title="À propos de nous"
  />
  {/* Banner area end */}
  <section id="main-container" className="main-container">
    <div className="container">
      <div className="row">
        <div className="col-md-6 align-self-md-center">
          <img src="assets/images/about/about_page_img1.png" alt className="img-fluid" />
        </div>
        {/* Col End */}
        <div className="col-md-6">
          <div className="tw-about-bin">
            <h2 className="column-title">À propos de nous</h2>
            <span className="animate-border tw-mb-40 tw-mt-20" />
            <p>Notre équipe est là pour vous accueillir !
Digital Supporter est une agence web spécialisée à taille humaine proche d’Aix en Provence et de Marseille, capable de vous suivre dans tous vos projets dans les domaines du web et du design graphique tout en respectant votre contexte et votre environnement.</p>
            <p>Notre objectif est de mettre au point des solutions de qualité qui nous permettent de répondre aux besoins, souvent très différents, que nous pouvons rencontrer chaque jour au contact de nos annonceurs depuis maintenant plusieurs années. </p>
          </div>
          {/* About Bin End */}
        </div>
        {/* Col End */}
      </div>
      {/* Row end */}
    </div>
    {/* Container end */}
  </section>
  {/* Main container end */}
  <section id="tw-about-award" className="tw-about-award">
    <div className="container">
      <div className="row">
        <div className="col-md-3">
          <div className="tw-award-box">
            <div className="award-icon">
              <img src="assets/images/about/award1.png" alt />
            </div>
            <h3>Prix du bon design 2020</h3>
          </div>
          {/* Award Box End */}
        </div>
        {/* Col End */}
        <div className="col-md-3">
          <div className="tw-award-box">
            <div className="award-icon">
              <img src="assets/images/about/award2.png" alt />
            </div>
            <h3>Prix CSS du meilleur<br /> site design</h3>
          </div>
          {/* Award Box End */}
        </div>
        {/* Col End */}
        <div className="col-md-3">
          <div className="tw-award-box">
            <div className="award-icon">
              <img src="assets/images/about/award3.png" alt />
            </div>
            <h3>Reddit meilleur référencement<br /> USA 2020</h3>
          </div>
          {/* Award Box End */}
        </div>
        {/* Col End */}
        <div className="col-md-3">
          <div className="tw-award-box">
            <div className="award-icon">
              <img src="assets/images/about/award4.png" alt />
            </div>
            <h3>Site des récompenses de la <br /> year 2020</h3>
          </div>
          {/* Award Box End */}
        </div>
        {/* Col End */}
      </div>
      {/* Row End */}
    </div>
    {/* Container End */}
  </section>
  {/* About award end */}
  <section id="tw-mission" className="tw-mission">
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <OwlCarousel className="mission-carousel owl-carousel"
          items={1}
   loop={true}
   nav={false}
   autoplay={true}
   dots={true}
   navText={['<i class="icon icon-left-arrow2"></i>','<i class="icon icon-right-arrow2"></i>']}
   
          >
            <div className="row">
              <div className="col-md-6 mr-auto align-self-md-center">
                <img src="assets/images/about/our_mission.png" alt className="img-fluid" />
              </div>
              {/* Col End */}
              <div className="col-md-6">
                <div className="mission-body">
                  <div className="mission-title tw-mb-40">
                    <h2 className="column-title">Notre mission</h2>
                    <span className="animate-border bg-white border-orange tw-mt-30" />
                  </div>
                  <p>
                  Notre équipe est un collectif de membres soudés, spécialistes dans les domaines du développement web, du design graphique, du webmarketing, du référencement naturel et de la gestion de projet.
                  </p>
                </div>
              </div>
              {/* Col End */}
            </div>
            {/* Content Row End */}
            <div className="row">
              <div className="col-md-6 mr-auto align-self-md-center">
                <img src="assets/images/about/our_mission.png" alt className="img-fluid" />
              </div>
              {/* Col End */}
              <div className="col-md-6">
                <div className="mission-body">
                  <div className="mission-title tw-mb-40">
                    <h2 className="column-title">Notre mission</h2>
                    <span className="animate-border bg-white border-orange tw-mt-30" />
                  </div>
                  <p>
                  Peut-être avez-vous aujourd’hui besoin d’un accompagnement complet pour un projet nécessitant du développement, une recherche graphique, une meilleure visibilité sur Google et un suivi webmarketing ?
Ou peut être avez-vous simplement besoin de l’essentiel et de vous garantir une certaine autonomie pour vous lancer en maîtrisant les risques ?
                  </p>
                </div>
              </div>
              {/* Col End */}
            </div>
            {/* Content Row End */}
            <div className="row animated fadeIn">
              <div className="col-md-6 mr-auto align-self-md-center">
                <img src="assets/images/about/our_mission.png" alt className="img-fluid" />
              </div>
              {/* Col End */}
              <div className="col-md-6">
                <div className="mission-body">
                  <div className="mission-title tw-mb-40">
                    <h2 className="column-title">Notre mission</h2>
                    <span className="animate-border bg-white border-orange tw-mt-30" />
                  </div>
                  <p>Dans tous les cas, notre équipe saura vous conseiller et vous proposer la solution adaptée à vos besoins, votre budget et vos objectifs.
                  </p>
                </div>
              </div>
              {/* Col End */}
            </div>
            {/* Content Row End */}
          </OwlCarousel>
          {/* Mission Carousel End */}
        </div>
        {/* Col End */}
      </div>
      {/* Row End */}
    </div>
    {/* Container End */}
  </section>
  {/* Mission End */}
  <section id="tw-team" className="tw-team" style={{display:'none'}}>
    <div className="container">
      <div className="row">
        <div className="col text-center">
          <div className="section-heading tw-mb-80">
            <h2>
              <small>our team</small>
              Our Best <span>Experts</span>
            </h2>
            <span className="animate-border tw-mt-20 ml-auto mr-auto" />
          </div>
        </div>
        {/* Title Col end */}
      </div>
      {/* Title row End */}
      <div className="row">
        <div className="col-md-3">
          <div className="tw-team-box">
            <div className="team-img">
              <img src="assets/images/team/expert1.png" alt className="img-fluid bg-green" />
            </div>
            <div className="team-info">
              <h3 className="team-name">Matthew Hunter</h3>
              <p className="team-designation">Marketing Head</p>
              <div className="team-social-links">
                <a href="#"><i className="fa fa-facebook" /></a>
                <a href="#"><i className="fa fa-twitter" /></a>
                <a href="#"><i className="fa fa-instagram" /></a>
                <a href="#"><i className="fa fa-linkedin" /></a>
              </div>
            </div>
            {/* Team Info end */}
          </div>
          {/* Team Box End */}
        </div>
        {/* Col End */}
        <div className="col-md-3">
          <div className="tw-team-box">
            <div className="team-img">
              <img src="assets/images/team/expert2.png" alt className="img-fluid bg-orange" />
            </div>
            <div className="team-info">
              <h3 className="team-name">Donna Estrada</h3>
              <p className="team-designation">Vice President</p>
              <div className="team-social-links">
                <a href="#"><i className="fa fa-facebook" /></a>
                <a href="#"><i className="fa fa-twitter" /></a>
                <a href="#"><i className="fa fa-instagram" /></a>
                <a href="#"><i className="fa fa-linkedin" /></a>
              </div>
            </div>
            {/* Team Info end */}
          </div>
          {/* Team Box End */}
        </div>
        {/* Col End */}
        <div className="col-md-3">
          <div className="tw-team-box">
            <div className="team-img">
              <img src="assets/images/team/expert3.png" alt className="img-fluid bg-blue" />
            </div>
            <div className="team-info">
              <h3 className="team-name">Harold Hopkins</h3>
              <p className="team-designation">Hiring Manager</p>
              <div className="team-social-links">
                <a href="#"><i className="fa fa-facebook" /></a>
                <a href="#"><i className="fa fa-twitter" /></a>
                <a href="#"><i className="fa fa-instagram" /></a>
                <a href="#"><i className="fa fa-linkedin" /></a>
              </div>
            </div>
            {/* Team Info end */}
          </div>
          {/* Team Box End */}
        </div>
        {/* Col End */}
        <div className="col-md-3">
          <div className="tw-team-box">
            <div className="team-img">
              <img src="assets/images/team/expert4.png" alt className="img-fluid bg-yellow" />
            </div>
            <div className="team-info">
              <h3 className="team-name">Rose Salazar</h3>
              <p className="team-designation">Advisor</p>
              <div className="team-social-links">
                <a href="#"><i className="fa fa-facebook" /></a>
                <a href="#"><i className="fa fa-twitter" /></a>
                <a href="#"><i className="fa fa-instagram" /></a>
                <a href="#"><i className="fa fa-linkedin" /></a>
              </div>
            </div>
            {/* Team Info end */}
          </div>
          {/* Team Box End */}
        </div>
        {/* Col End */}
      </div>
      {/* Content Row End */}
    </div>
    {/* Container end */}
  </section>
  {/* Team Section End */}
          <Comment/>
  {/* End TW testimonial */}
  <section id="tw-client" className="tw-client">
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <OwlCarousel className="clients-carousel owl-carousel"
          items={4}
   loop={true}
   nav={false}
   autoplay={true}
   dots={true}
          >
            <div className="client-logo-wrapper d-table">
              <div className="client-logo d-table-cell">
                <img src="assets/images/clients/client1.png" alt />
              </div>
              {/* End Clients logo */}
            </div>
            {/* End Client wrapper */}
            <div className="client-logo-wrapper d-table">
              <div className="client-logo d-table-cell">
                <img src="assets/images/clients/client2.png" alt />
              </div>
              {/* End Clients logo */}
            </div>
            {/* End Client wrapper */}
            <div className="client-logo-wrapper d-table">
              <div className="client-logo d-table-cell">
                <img src="assets/images/clients/client3.png" alt />
              </div>
              {/* End Clients logo */}
            </div>
            {/* End Client wrapper */}
            <div className="client-logo-wrapper d-table">
              <div className="client-logo d-table-cell">
                <img src="assets/images/clients/client4.png" alt />
              </div>
              {/* End Clients logo */}
            </div>
            {/* End Client wrapper */}
            <div className="client-logo-wrapper d-table">
              <div className="client-logo d-table-cell">
                <img src="assets/images/clients/client2.png" alt />
              </div>
              {/* End Clients logo */}
            </div>
            {/* End Client wrapper */}
            <div className="client-logo-wrapper d-table">
              <div className="client-logo d-table-cell">
                <img src="assets/images/clients/client2.png" alt />
              </div>
              {/* End Clients logo */}
            </div>
            {/* End Client wrapper */}
            <div className="client-logo-wrapper d-table">
              <div className="client-logo d-table-cell">
                <img src="assets/images/clients/client2.png" alt />
              </div>
              {/* End Clients logo */}
            </div>
            {/* End Client wrapper */}
            <div className="client-logo-wrapper d-table">
              <div className="client-logo d-table-cell">
                <img src="assets/images/clients/client2.png" alt />
              </div>
              {/* End Clients logo */}
            </div>
            {/* End Client wrapper */}
            <div className="client-logo-wrapper d-table">
              <div className="client-logo d-table-cell">
                <img src="assets/images/clients/client2.png" alt />
              </div>
              {/* End Clients logo */}
            </div>
            {/* End Client wrapper */}
          </OwlCarousel>
          {/* End Carousel */}
        </div>
        {/* End Col */}
      </div>
      {/* End Row */}
    </div>
    {/* End Container */}
  </section>
  {/* End Tw Client */}
</>

	
        )
    }
}
export default About