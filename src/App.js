import React from 'react';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'; 
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Home from './components/Home';
import Footer from './components/partial/Footer';
import About from './components/About';
import Services from './components/Services';
import Single from './components/Single';
import Message from './components/Message';
import Contact from './components/Contact';
import Career from './components/Career';
import AllBlog from './components/AllBlog';
import NewsSingle from './components/NewsSingle';
import Testimonials from './components/Testimonials';
import Category from './components/Category';
import Cart from './components/Cart';
import Wishlist from './components/Wishlist';
import Settings from './components/Settings';
import SubCategory from './components/SubCategory';
import Terms from './components/Terms';
import Login from './components/Login';
import Auth from './components/Auth';

function App() {
  return (
    <div>
      <Router>
        {/* <Navbar/> */}
          <Switch>
            <Route exact path='/'component={Home}/>
            <Route path='/newsSingle:pid' component={NewsSingle}/>
            <Route path='/category' component={Category}/>
            <Route path='/subcategory' component={SubCategory}/>
            <Route path='/cart' component={Cart}/>
            <Route path='/wishlist' component={Wishlist}/>
            <Route path='/Settings' component={Settings}/>
            <Route path='/about' component={About}/>
            <Route path='/career' component={Career}/>
            <Route path='/services' component={Services}/>
            <Route path='/single:pid' component={Single}/> 
            <Route path='/message' component={Message}/>
            <Route path='/login' component={Login}/>
            <Route path='/allblog' component={AllBlog}/>
            <Route path='/contact' component={Contact}/>
            <Route path='/testimonials' component={Testimonials}/>
            <Route path='/terms' component={Terms}/>
            <Route path='/auth' component={Auth}/>

          </Switch>
        {/* <Footer/> */}
      </Router>
      
    </div>
  );
}

export default App;
